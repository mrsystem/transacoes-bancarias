FROM adoptopenjdk/openjdk14:alpine

COPY adaptadores/web/target/*.jar application.jar

CMD ["java", "-jar", "-Dspring.profiles.active=${ENVIRONMENT_PROFILE}", "application.jar"]
