package br.projeto.transacoes.adaptadores.newpersistence.cliente;

import br.projeto.transacoes.adaptadores.newpersistence.conta.ContaBancariaEntity;
import br.projeto.transacoes.domain.cliente.Cliente;
import br.projeto.transacoes.domain.cliente.SituacaoCliente;

import javax.persistence.*;
import java.util.Collection;
import java.util.UUID;
import java.util.stream.Collectors;

@SuppressWarnings("unused")
@Entity
@Table(name = "cliente")
public class ClienteEntity {

    private ClienteEntity() {
    }

    ClienteEntity(final Cliente cliente) {

        cpf = cliente.obterCpf();

        situacaoCliente = cliente.obterSituacao().toInt();

        contasBancaria = cliente
                .obterContasBancaria()
                .stream()
                .map(contaBancaria -> new ContaBancariaEntity(contaBancaria, this))
                .collect(Collectors.toUnmodifiableSet());

    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    private String cpf;

    @Column(name = "situacao")
    private int situacaoCliente;

    @OneToMany(mappedBy = "clienteEntity", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Collection<ContaBancariaEntity> contasBancaria;

    Cliente toCliente() {

        return Cliente
                .builder()
                .situacao(SituacaoCliente.valueOf(situacaoCliente))
                .possuiContas
                        (
                                contasBancaria
                                        .stream()
                                        .map(ContaBancariaEntity::toContaBancaria)
                                        .collect(Collectors.toUnmodifiableSet())
                        )
                .comCpf(cpf)
                .build();

    }

}
