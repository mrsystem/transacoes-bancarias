package br.projeto.transacoes.adaptadores.newpersistence.transacao;

import br.projeto.transacoes.adaptadores.newpersistence.conta.ContaBancariaEntity;
import br.projeto.transacoes.domain.transacao.CategoriaTransacao;
import br.projeto.transacoes.domain.transacao.Transacao;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

@SuppressWarnings("unused")
@Entity
@Table(name = "transacao")
public class TransacaoEntity {

    private TransacaoEntity() {
    }

    TransacaoEntity(final Transacao transacao, final ContaBancariaEntity contaBancariaEntity) {
        dataHoraEfetivacao = transacao.obterDataHoraEfetivacao();
        autenticacao = transacao.obterAutenticacao();
        valor = transacao.obterValor();
        descricao = transacao.obterDescricao();
        categoriaTransacao = transacao.obterCategoria().toInt();
        this.contaBancariaEntity = contaBancariaEntity;
    }

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private UUID id;

    private LocalDateTime dataHoraEfetivacao;

    private String autenticacao;

    private BigDecimal valor;

    private String descricao;

    @Column(name = "categoria")
    private int categoriaTransacao;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "conta_bancaria_id", referencedColumnName = "id")
    private ContaBancariaEntity contaBancariaEntity;

    Transacao toTransacao() {
        return Transacao
                .builder(dataHoraEfetivacao)
                .comValor(valor)
                .suaCategoria(CategoriaTransacao.valueOf(categoriaTransacao))
                .comDescricao(descricao)
                .comAutenticacao(autenticacao)
                .paraContaBancaria(contaBancariaEntity.toContaBancaria())
                .build();
    }

}
