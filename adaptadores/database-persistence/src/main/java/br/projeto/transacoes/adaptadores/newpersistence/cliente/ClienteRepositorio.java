package br.projeto.transacoes.adaptadores.newpersistence.cliente;

import java.util.Optional;

public interface ClienteRepositorio {

    Optional<ClienteEntity> findByCpf(final String cpf);

    void save(final ClienteEntity clienteEntity);

}
