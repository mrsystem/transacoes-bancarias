package br.projeto.transacoes.adaptadores.newpersistence.conta;

import br.projeto.transacoes.adaptadores.newpersistence.cliente.ClienteEntity;
import br.projeto.transacoes.domain.conta.ContaBancaria;

import javax.persistence.*;
import java.util.UUID;

@SuppressWarnings("unused")
@Entity
@Table(name = "conta_bancaria")
public class ContaBancariaEntity {

    private ContaBancariaEntity() {
    }

    public ContaBancariaEntity(final ContaBancaria contaBancaria, final ClienteEntity clienteEntity) {
        agencia = contaBancaria.getAgencia();
        contaCorrente = contaBancaria.getContaCorrente();
        this.clienteEntity = clienteEntity;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cliente_id")
    private ClienteEntity clienteEntity;

    private String agencia;

    private String contaCorrente;

    public ContaBancaria toContaBancaria() {
        return ContaBancaria
                .builder()
                .paraAgencia(agencia)
                .comContaCorrente(contaCorrente)
                .build();
    }

}
