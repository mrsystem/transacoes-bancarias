package br.projeto.transacoes.adaptadores.newpersistence.cliente;

import br.projeto.transacoes.domain.cliente.Cliente;
import br.projeto.transacoes.domain.cliente.ClienteGateway;

import java.util.Optional;

public final class ClienteQueryNewDatabaseAdapter implements ClienteGateway {

    public ClienteQueryNewDatabaseAdapter(final ClienteRepositorio clienteRepositorio) {
        this.clienteRepositorio = clienteRepositorio;
    }

    private final ClienteRepositorio clienteRepositorio;

    @Override
    public Optional<Cliente> recuperarPorCpf(final String cpf) {

        return clienteRepositorio
                .findByCpf(cpf)
                .map(ClienteEntity::toCliente);

    }

    @Override
    public void salvar(final Cliente cliente) {
        clienteRepositorio.save(new ClienteEntity(cliente));
    }
}

