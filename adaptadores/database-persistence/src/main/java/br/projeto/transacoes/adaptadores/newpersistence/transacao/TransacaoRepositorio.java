package br.projeto.transacoes.adaptadores.newpersistence.transacao;

import java.util.Collection;
import java.util.Set;

public interface TransacaoRepositorio {

    Collection<TransacaoEntity> transacoesPorAgenciaEContaCorrente
            (final String agencia, final String contaCorrente);

    void salvarTodos(final Set<TransacaoEntity> transactions);

}
