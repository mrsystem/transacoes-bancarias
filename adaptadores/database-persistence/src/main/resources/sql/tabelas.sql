create table cliente
(

    id       uuid               not null primary key,

    cpf      varchar(14) unique not null,

    situacao int                not null default 0 check (situacao >= 0 and situacao < 5)

);

create table conta_bancaria
(

    id             uuid        not null primary key,

    cliente_id     uuid        not null,

    agencia        varchar(10) not null,

    conta_corrente varchar(10) not null,

    constraint AGENCIA_CONTA unique (agencia, conta_corrente),

    foreign key (cliente_id) references cliente (id)

);

create table transacao
(

    id                   uuid         not null primary key,

    conta_bancaria_id    uuid         not null,

    data_hora_efetivacao timestamp    not null,

    autenticacao         varchar(64)  not null unique,

    valor                decimal      not null,

    descricao            varchar(255) not null,

    categoria            int          not null check (categoria >= 0),

    foreign key (conta_bancaria_id) references conta_bancaria (id)

);