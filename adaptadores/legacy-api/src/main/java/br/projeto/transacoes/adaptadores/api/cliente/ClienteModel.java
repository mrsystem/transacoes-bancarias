package br.projeto.transacoes.adaptadores.api.cliente;

import br.projeto.transacoes.domain.cliente.Cliente;
import br.projeto.transacoes.domain.cliente.Cliente.ClienteBuilder;
import br.projeto.transacoes.domain.cliente.SituacaoCliente;
import br.projeto.transacoes.domain.conta.ContaBancaria;
import br.projeto.transacoes.domain.conta.ContaBancaria.ContaBancariaBuilder;

import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings("unused")
final class ClienteModel {

    private static class ContaBancariaModel {

        private ContaBancariaModel() {
            builder = ContaBancaria.builder();
        }

        private final ContaBancariaBuilder builder;

        private void setAgencia(final String agencia) {
            builder.paraAgencia(agencia);
        }

        private void setContaCorrente(final String contaCorrente) {
            builder.comContaCorrente(contaCorrente);
        }

        private ContaBancaria toContaBancaria() {
            return builder.build();
        }

    }

    private ClienteModel() {
        builder = Cliente.builder();
    }

    private final ClienteBuilder builder;

    private void setCpf(final String cpf) {
        builder.comCpf(cpf);
    }

    private void setSituacaoCliente(final int situacaoCliente) {
        builder.situacao(SituacaoCliente.valueOf(situacaoCliente));
    }

    private void setContasBancaria(final List<ContaBancariaModel> contasBancaria) {
        builder
                .possuiContas
                        (
                                contasBancaria
                                        .stream()
                                        .map(ContaBancariaModel::toContaBancaria)
                                        .collect(Collectors.toUnmodifiableSet())
                        );
    }

    Cliente toCliente() {
        return builder.build();
    }

}
