package br.projeto.transacoes.adaptadores.api.cliente;

import br.projeto.transacoes.domain.cliente.Cliente;
import br.projeto.transacoes.domain.cliente.ClienteQuery;

import java.util.Optional;

public final class ClienteQueryAPIAdapter implements ClienteQuery {

    public ClienteQueryAPIAdapter(final ClienteAPI clienteAPI) {
        this.clienteAPI = clienteAPI;
    }

    private final ClienteAPI clienteAPI;

    @Override
    public Optional<Cliente> recuperarPorCpf(final String cpf) {
        return clienteAPI.recuperarPorCPF(cpf).map(ClienteModel::toCliente);
    }

}
