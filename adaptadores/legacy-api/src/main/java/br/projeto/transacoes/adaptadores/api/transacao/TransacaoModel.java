package br.projeto.transacoes.adaptadores.api.transacao;

import br.projeto.transacoes.domain.conta.ContaBancaria;
import br.projeto.transacoes.domain.transacao.CategoriaTransacao;
import br.projeto.transacoes.domain.transacao.Transacao;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@SuppressWarnings("unused")
class TransacaoModel {

    private TransacaoModel() {
    }

    private LocalDateTime dataHoraEfetivacao;

    private void setDataHoraEfetivacao(final LocalDateTime dataHoraEfetivacao) {
        this.dataHoraEfetivacao = dataHoraEfetivacao;
    }

    private String autenticacao;

    private void setAutenticacao(final String autenticacao) {
        this.autenticacao = autenticacao;
    }

    private BigDecimal valor;

    private void setValor(final BigDecimal valor) {
        this.valor = valor;
    }

    private String descricao;

    private void setDescricao(final String descricao) {
        this.descricao = descricao;
    }

    private CategoriaTransacao categoria;

    private void setCategoria(final int categoria) {
        this.categoria = CategoriaTransacao.valueOf(categoria);
    }

    Transacao toTransacao(final ContaBancaria contaBancaria) {
        return Transacao
                .builder(dataHoraEfetivacao)
                .paraContaBancaria(contaBancaria)
                .comAutenticacao(autenticacao)
                .comDescricao(descricao)
                .suaCategoria(categoria)
                .comValor(valor)
                .build();
    }

}
