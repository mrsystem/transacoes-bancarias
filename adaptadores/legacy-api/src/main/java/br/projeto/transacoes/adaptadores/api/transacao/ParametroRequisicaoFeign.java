package br.projeto.transacoes.adaptadores.api.transacao;

import br.projeto.transacoes.domain.conta.ContaBancaria;

@SuppressWarnings("FieldCanBeLocal")
public final class ParametroRequisicaoFeign {

    ParametroRequisicaoFeign(final ContaBancaria contaBancaria) {
        agencia = contaBancaria.getAgencia();
        conta = contaBancaria.getContaCorrente();
    }

    @SuppressWarnings("unused")
	private final String agencia;

    @SuppressWarnings("unused")
	private final String conta;

}
