package br.projeto.transacoes.adaptadores.web.model;

import br.projeto.transacoes.aplicacao.resposta.Erro;

@SuppressWarnings("unused")
public final class ErroModel {

    public ErroModel(final Erro erro) {
        this.erro = erro;
    }

    private final Erro erro;

    public String getValor() {
        return erro.obterValor();
    }

    public String getCampo() {
        return erro.obterCampo();
    }

    public String getMensagem() {
        return erro.obterMensagem();
    }

    public int getCodigo() {
        return erro.obterCodigoErro();
    }

}
