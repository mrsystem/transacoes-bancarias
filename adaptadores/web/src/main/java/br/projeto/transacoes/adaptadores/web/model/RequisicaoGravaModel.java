package br.projeto.transacoes.adaptadores.web.model;

import br.projeto.transacoes.aplicacao.grava.RequisicaoClienteComTransacoes;
import br.projeto.transacoes.aplicacao.grava.RequisicaoContaBancaria;
import br.projeto.transacoes.aplicacao.grava.RequisicaoTransacao;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.stream.Collectors;

@SuppressWarnings("unused")
public class RequisicaoGravaModel implements RequisicaoClienteComTransacoes {

    private static final class RequisicaoContaBancariaWeb implements RequisicaoContaBancaria {

        private static final class RequisicaoTransacaoWeb implements RequisicaoTransacao {

            private RequisicaoTransacaoWeb() {
            }

            private LocalDateTime dataHoraEfetivacao;

            @Override
            public LocalDateTime obterDataHoraEfetivacao() {
                return dataHoraEfetivacao;
            }

            private void setDataHoraEfetivacao(final LocalDateTime dataHoraEfetivacao) {
                this.dataHoraEfetivacao = dataHoraEfetivacao;
            }

            private String autenticacao;

            private void setAutenticacao(final String autenticacao) {
                this.autenticacao = autenticacao;
            }

            @Override
            public String obterAutenticacao() {
                return autenticacao;
            }

            private BigDecimal valor;

            private void setValor(final BigDecimal valor) {
                this.valor = valor;
            }

            @Override
            public BigDecimal obterValor() {
                return valor;
            }

            private int categoria;

            private void setCategoria(final int categoria) {
                this.categoria = categoria;
            }

            @Override
            public int obterCategoriaTransacao() {
                return categoria;
            }

            private String descricao;

            private void setDescricao(final String descricao) {
                this.descricao = descricao;
            }

            @Override
            public String obterDescricao() {
                return descricao;
            }

        }

        private RequisicaoContaBancariaWeb() {
        }

        private void setAgencia(final String agencia) {
            this.agencia = agencia;
        }

        private String agencia;

        @Override
        public String obterAgencia() {
            return agencia;
        }

        private String contaCorrente;

        private void setContaCorrente(final String contaCorrente) {
            this.contaCorrente = contaCorrente;
        }

        @Override
        public String obterContaCorrente() {
            return contaCorrente;
        }

        @NotNull
        private Collection<RequisicaoTransacao> transacoes;

        private void setTransacoes(final Collection<RequisicaoTransacaoWeb> transacoes) {
            this.transacoes = transacoes.stream().collect(Collectors.toUnmodifiableSet());
        }

        @Override
        public Collection<RequisicaoTransacao> obterRequisicaoTransacoes() {
            return transacoes;
        }

    }

    private RequisicaoGravaModel() {
    }

    private String cpf;

    @Override
    public String obterCpf() {
        return cpf;
    }

    private void setCpf(final String cpf) {
        this.cpf = cpf;
    }

    @NotNull
    private Collection<RequisicaoContaBancaria> contasBancarias;

    private void setContasBancarias(final Collection<RequisicaoContaBancariaWeb> contasBancarias) {
        this.contasBancarias = contasBancarias.stream().collect(Collectors.toUnmodifiableSet());
    }

    @Override
    public Collection<RequisicaoContaBancaria> obterContasBancaria() {
        return contasBancarias;
    }

}
