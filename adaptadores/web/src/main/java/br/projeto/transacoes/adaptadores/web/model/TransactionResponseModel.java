package br.projeto.transacoes.adaptadores.web.model;

import br.projeto.transacoes.aplicacao.resposta.Erro;
import br.projeto.transacoes.aplicacao.consulta.RespostaConsulta;
import br.projeto.transacoes.aplicacao.consulta.RespostaTransacao;
import br.projeto.transacoes.domain.transacao.CategoriaTransacao;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

@SuppressWarnings("unused")
public class TransactionResponseModel {

    private static class RespostaTransacaoWeb {

        private RespostaTransacaoWeb(final RespostaTransacao respostaTransacao) {
            this.respostaTransacao = respostaTransacao;
        }

        private final RespostaTransacao respostaTransacao;

        public LocalDateTime getDataHoraTransacao() {
            return respostaTransacao.obterDataHoraTransacao();
        }

        public String getDescricao() {
            return respostaTransacao.obterDescricao();
        }

        public BigDecimal getValor() {
            return respostaTransacao.obterValor();
        }

    }

    public static TransactionResponseModel of(final RespostaConsulta respostaConsulta) {
        return new TransactionResponseModel(respostaConsulta.obterErros(), respostaConsulta.obterTransacoes());
    }

    private TransactionResponseModel
            (
                    final Set<Erro> erros,
                    final Map<CategoriaTransacao, Set<RespostaTransacao>> transactions
            ) {
        this.erros = erros;
        this.transactions = transactions;
    }

    private final Set<Erro> erros;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Set<ErroModel> getErros() {
        return erros
                .stream()
                .map(ErroModel::new)
                .collect(Collectors.toUnmodifiableSet());
    }

    private final Map<CategoriaTransacao, Set<RespostaTransacao>> transactions;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public ConcurrentMap<String, Set<RespostaTransacaoWeb>> getTransacoes() {
        return transactions
                .entrySet()
                .parallelStream()
                .collect
                        (
                                Collectors
                                        .toConcurrentMap
                                                (
                                                        categoriaTransacaoSetEntry ->
                                                                categoriaTransacaoSetEntry
                                                                        .getKey()
                                                                        .name(),
                                                        transactions -> transactions.getValue()
                                                                .stream()
                                                                .map(RespostaTransacaoWeb::new)
                                                                .collect(Collectors.toCollection(LinkedHashSet::new))
                                                )
                        );
    }

}
