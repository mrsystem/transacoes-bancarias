package br.projeto.transacoes.adaptadores.web.configuration;

import br.project.knin.domain.DomainGateway;
import br.project.knin.domain.DomainQuery;
import br.project.knin.domain.chain.DomainChainBuilder;
import br.projeto.transacoes.adaptadores.newpersistence.cliente.ClienteQueryNewDatabaseAdapter;
import br.projeto.transacoes.adaptadores.newpersistence.cliente.ClienteRepositorio;
import br.projeto.transacoes.domain.cliente.Cliente;
import br.projeto.transacoes.domain.cliente.ClienteGateway;
import br.projeto.transacoes.domain.cliente.ClienteQuery;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class ClienteConfiguration {

    @Bean
    ClienteGateway clienteGateway(final ClienteRepositorio repository) {
        return new ClienteQueryNewDatabaseAdapter(repository);
    }

    @Bean
    ClienteQuery clienteQuery
            (
                    @Qualifier("new-cliente-database") final DomainGateway<String, Cliente> clienteRepositoryOfNewDatabase,
                    @Qualifier("legacy-cliente-api") final DomainQuery<String, Cliente> clienteOfLegacyAPI
            ) {

        return
                DomainChainBuilder
                        .<String, Cliente>
                                builder()
                        .add(clienteRepositoryOfNewDatabase)
                        .add(clienteOfLegacyAPI)
                        .build()
                        ::recover;

    }

}
