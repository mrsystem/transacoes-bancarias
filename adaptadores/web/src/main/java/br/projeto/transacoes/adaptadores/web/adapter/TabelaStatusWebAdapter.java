package br.projeto.transacoes.adaptadores.web.adapter;

import br.projeto.transacoes.aplicacao.resposta.TabelaStatus;
import org.springframework.http.HttpStatus;

public final class TabelaStatusWebAdapter implements TabelaStatus {

    @Override
    public int statusRequestInvalid() {
        return HttpStatus.BAD_REQUEST.value();
    }

    @Override
    public int statusCustomerNotFound() {
        return HttpStatus.NOT_FOUND.value();
    }

    @Override
    public int statusCustomerNotAuthorized() {
        return HttpStatus.NON_AUTHORITATIVE_INFORMATION.value();
    }

    @Override
    public int statusBalanceAccountNotFound() {
        return HttpStatus.NOT_FOUND.value();
    }

    @Override
    public int statusTransactionNotFound() {
        return HttpStatus.NOT_FOUND.value();
    }

    @Override
    public int statusSuccess() {
        return HttpStatus.OK.value();
    }

    @Override
    public int statusInternalError() {
        return HttpStatus.INTERNAL_SERVER_ERROR.value();
    }

}
