package br.projeto.transacoes.adaptadores.web.repository;

import br.projeto.transacoes.adaptadores.newpersistence.conta.ContaBancariaEntity;
import br.projeto.transacoes.adaptadores.newpersistence.conta.ContaBancariaRepositorio;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface ContaBancariaJPARepositorio extends JpaRepository<ContaBancariaEntity, UUID> {

    final class ContaBancariaRepositorioAdaptador implements ContaBancariaRepositorio {

        public ContaBancariaRepositorioAdaptador(final ContaBancariaJPARepositorio contaBancariaJPARepositorio) {
            this.contaBancariaJPARepositorio = contaBancariaJPARepositorio;
        }

        private final ContaBancariaJPARepositorio contaBancariaJPARepositorio;

        @Override
        public Optional<ContaBancariaEntity> encontrarPorAgenciaEContaCorrente(final String agencia, final String contaCorrente) {
            return contaBancariaJPARepositorio.findByAgenciaAndContaCorrente(agencia, contaCorrente);
        }
    }

    Optional<ContaBancariaEntity> findByAgenciaAndContaCorrente(final String agencia, final String contaCorrente);

}
