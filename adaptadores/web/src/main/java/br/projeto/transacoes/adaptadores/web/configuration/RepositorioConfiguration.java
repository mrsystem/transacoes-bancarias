package br.projeto.transacoes.adaptadores.web.configuration;

import br.project.knin.domain.DomainGateway;
import br.projeto.transacoes.adaptadores.newpersistence.cliente.ClienteRepositorio;
import br.projeto.transacoes.adaptadores.newpersistence.conta.ContaBancariaRepositorio;
import br.projeto.transacoes.adaptadores.newpersistence.transacao.TransacaoRepositorio;
import br.projeto.transacoes.adaptadores.web.repository.ClienteJPARepositorio;
import br.projeto.transacoes.adaptadores.web.repository.ContaBancariaJPARepositorio;
import br.projeto.transacoes.adaptadores.web.repository.TransacaoJPARepositorio;
import br.projeto.transacoes.domain.cliente.Cliente;
import br.projeto.transacoes.domain.cliente.ClienteGateway;
import br.projeto.transacoes.domain.conta.ContaBancaria;
import br.projeto.transacoes.domain.transacao.Transacao;
import br.projeto.transacoes.domain.transacao.TransacaoGateway;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Set;

@Configuration
@EntityScan(basePackages = "br.projeto.transacoes.adaptadores.newpersistence")
class RepositorioConfiguration {

    @Bean
    ClienteRepositorio clienteRepositorioAdapter(final ClienteJPARepositorio clienteJPARepositorio) {
        return new ClienteJPARepositorio.ClienteRepositorioAdapter(clienteJPARepositorio);
    }

    @Bean(name = "new-cliente-database")
    DomainGateway<String, Cliente> clienteDomainGatewayNewDatabase
            (
                    final ClienteGateway clienteGateway
            ) {

        return DomainGateway.create(clienteGateway::recuperarPorCpf, clienteGateway::salvar);

    }

    @Bean
    TransacaoRepositorio transacaoRepositorio(final TransacaoJPARepositorio transacaoJPARepositorio) {
        return new TransacaoJPARepositorio.TransacaoRepositorioAdapter(transacaoJPARepositorio);
    }

    @Bean
    ContaBancariaRepositorio contaBancariaRepositorio(final ContaBancariaJPARepositorio contaBancariaJPARepositorio) {
        return new ContaBancariaJPARepositorio.ContaBancariaRepositorioAdaptador(contaBancariaJPARepositorio);
    }

    @Bean(name = "new-transaction-database")
    DomainGateway<ContaBancaria, Set<Transacao>> transactionDomainGatewayNewDatabase
            (
                    final TransacaoGateway transacaoGateway
            ) {

        return DomainGateway.create
                (
                        contaBancaria -> JCollections.toOptional(transacaoGateway.listar(contaBancaria)),
                        transacaoGateway::salvar
                );

    }

}
