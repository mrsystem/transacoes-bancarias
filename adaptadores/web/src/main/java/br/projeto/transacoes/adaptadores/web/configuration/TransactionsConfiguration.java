package br.projeto.transacoes.adaptadores.web.configuration;

import br.project.knin.domain.DomainGateway;
import br.project.knin.domain.DomainQuery;
import br.project.knin.domain.chain.DomainChainBuilder;
import br.projeto.transacoes.adaptadores.newpersistence.conta.ContaBancariaRepositorio;
import br.projeto.transacoes.adaptadores.newpersistence.transacao.TransacaoRepositorio;
import br.projeto.transacoes.adaptadores.newpersistence.transacao.TransactionNewDatabaseFacade;
import br.projeto.transacoes.domain.conta.ContaBancaria;
import br.projeto.transacoes.domain.transacao.Transacao;
import br.projeto.transacoes.domain.transacao.TransacaoGateway;
import br.projeto.transacoes.domain.transacao.TransacaoQuery;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Collections;
import java.util.Set;

@Configuration
class TransactionsConfiguration {

    @Bean
    TransacaoGateway transacaoGateway
            (
                    final TransacaoRepositorio transactionRepository,
                    final ContaBancariaRepositorio contaBancariaRepository
            ) {
        return new TransactionNewDatabaseFacade(transactionRepository, contaBancariaRepository);
    }

    @Bean
    TransacaoQuery transacaoQuery
            (
                    @Qualifier("new-transaction-database") final DomainGateway<ContaBancaria, Set<Transacao>> transactionsRepositoryOfNewDatabase,
                    @Qualifier("legacy-transaction-api") final DomainQuery<ContaBancaria, Set<Transacao>> transactionsOfLegacyAPI
            ) {
        return
                contaBancaria ->
                        DomainChainBuilder
                                .<ContaBancaria, Set<Transacao>>
                                        builder()
                                .add(transactionsRepositoryOfNewDatabase)
                                .add(transactionsOfLegacyAPI)
                                .build()
                                .recover(contaBancaria)
                                .orElse(Collections.emptySet());

    }

}
