package br.projeto.transacoes.adaptadores.web.model;

import br.projeto.transacoes.aplicacao.consulta.RequisicaoConsulta;

public class RequisicaoConsultaModel {

    public static RequisicaoConsultaModel builder() {
        return new RequisicaoConsultaModel();
    }

    private RequisicaoConsultaModel() {
    }

    private String cpf;

    public RequisicaoConsultaModel comCpf(final String cpf) {
        this.cpf = cpf;
        return this;
    }

    private String agencia;

    public RequisicaoConsultaModel daAgencia(final String agencia) {
        this.agencia = agencia;
        return this;
    }

    private String contaCorrente;

    public RequisicaoConsultaModel comContaCorrente(final String contaCorrente) {
        this.contaCorrente = contaCorrente;
        return this;
    }

    private String initialDate;

    private String finalDate;

    public RequisicaoConsultaModel paraPeriodo(final String initialDate, final String finalDate) {
        this.initialDate = initialDate;
        this.finalDate = finalDate;
        return this;
    }

    public RequisicaoConsulta build() {
        return new RequisicaoConsultaWeb(this);
    }

    private static class RequisicaoConsultaWeb implements RequisicaoConsulta {

        private RequisicaoConsultaWeb(final RequisicaoConsultaModel requisicaoConsultaModel) {
            this.requisicaoConsultaModel = requisicaoConsultaModel;
        }

        private final RequisicaoConsultaModel requisicaoConsultaModel;

        @Override
        public String obterCpf() {
            return requisicaoConsultaModel.cpf;
        }

        @Override
        public String obterAgencia() {
            return requisicaoConsultaModel.agencia;
        }

        @Override
        public String obterContaCorrente() {
            return requisicaoConsultaModel.contaCorrente;
        }

        @Override
        public String obterDataInicio() {
            return requisicaoConsultaModel.initialDate;
        }

        @Override
        public String obterDataFim() {
            return requisicaoConsultaModel.finalDate;
        }
    }

}
