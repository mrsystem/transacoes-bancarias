package br.projeto.transacoes.adaptadores.web.repository;

import br.projeto.transacoes.adaptadores.newpersistence.cliente.ClienteEntity;
import br.projeto.transacoes.adaptadores.newpersistence.cliente.ClienteRepositorio;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface ClienteJPARepositorio extends JpaRepository<ClienteEntity, UUID> {

    final class ClienteRepositorioAdapter implements ClienteRepositorio {

        public ClienteRepositorioAdapter(final ClienteJPARepositorio clienteJPARepositorio) {
            this.clienteJPARepositorio = clienteJPARepositorio;
        }

        private final ClienteJPARepositorio clienteJPARepositorio;

        @Override
        public Optional<ClienteEntity> findByCpf(final String cpf) {
            return clienteJPARepositorio.findByCpf(cpf);
        }

        @Override
        public void save(final ClienteEntity clienteEntity) {
            clienteJPARepositorio.save(clienteEntity);
        }

    }

    Optional<ClienteEntity> findByCpf(final String cpf);

}
