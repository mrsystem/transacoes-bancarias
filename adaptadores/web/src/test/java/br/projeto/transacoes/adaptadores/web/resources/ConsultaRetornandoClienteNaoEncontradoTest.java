package br.projeto.transacoes.adaptadores.web.resources;

import br.projeto.transacoes.domain.cliente.Cliente;
import br.projeto.transacoes.domain.conta.ContaBancaria;
import com.github.tomakehurst.wiremock.client.WireMock;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.ResourceUtils;

import java.nio.file.Files;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@AutoConfigureWireMock(port = 0)
@ActiveProfiles("test")
class ConsultaRetornandoClienteNaoEncontradoTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void deveRetornarClienteNaoEncontrado() throws Exception {

        final Cliente cliente =
                Cliente
                        .builder(Cliente.mock())
                        .comCpf("97436350099")
                        .build();

        WireMock
                .stubFor
                        (
                                WireMock
                                        .get("/clientes/".concat(cliente.obterCpf()))
                                        .withHeader("Content-Type", WireMock.equalTo(MediaType.APPLICATION_JSON_VALUE))
                                        .willReturn
                                                (
                                                        aResponse()
                                                                .withHeader("Content-type", MediaType.APPLICATION_JSON_VALUE)
                                                                .withStatus(HttpStatus.NOT_FOUND.value())

                                                )
                        );

        final ContaBancaria contaBancaria = cliente.obterContasBancaria().iterator().next();

        final LinkedMultiValueMap<String, String> valueMap = new LinkedMultiValueMap<>();

        valueMap.add("agencia", contaBancaria.getAgencia());
        valueMap.add("conta", contaBancaria.getContaCorrente());
        valueMap.add("data_inicio", "01/01/2019");
        valueMap.add("data_fim", "01/01/2021");

        final String json = new String
                (
                        Files
                                .readAllBytes
                                        (
                                                ResourceUtils
                                                        .getFile("classpath:__files/json/cliente-nao-encontrado.json")
                                                        .toPath()
                                        )
                );

        mockMvc
                .perform
                        (
                                get("/clientes/{cpf}/transactions", cliente.obterCpf())
                                        .params(valueMap)
                        )
                .andDo(print())
                .andExpect(header().stringValues("Content-Type", MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.erros.[*]").isArray())
                .andExpect(jsonPath("$.erros.[*]", hasSize(1)))
                .andExpect(content().json(json));

    }

}
