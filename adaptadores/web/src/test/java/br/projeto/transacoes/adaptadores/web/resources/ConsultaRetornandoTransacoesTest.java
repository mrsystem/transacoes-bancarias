package br.projeto.transacoes.adaptadores.web.resources;

import br.projeto.transacoes.domain.cliente.Cliente;
import br.projeto.transacoes.domain.conta.ContaBancaria;
import com.github.tomakehurst.wiremock.client.WireMock;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.ResourceUtils;

import java.nio.file.Files;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@AutoConfigureWireMock(port = 0)
@ActiveProfiles("test")
class ConsultaRetornandoTransacoesTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void deveRetornarUmaTransacaoComResposta() throws Exception {

        final Cliente cliente = Cliente.mock();

        WireMock
                .stubFor
                        (
                                WireMock
                                        .get("/clientes/".concat(cliente.obterCpf()))
                                        .withHeader("Content-Type", WireMock.equalTo(MediaType.APPLICATION_JSON_VALUE))
                                        .willReturn
                                                (
                                                        aResponse()
                                                                .withHeader("Content-type", MediaType.APPLICATION_JSON_VALUE)
                                                                .withBodyFile("json/cliente.json")

                                                )
                        );

        final ContaBancaria contaBancaria = cliente.obterContasBancaria().iterator().next();

        WireMock
                .stubFor
                        (
                                WireMock
                                        .get
                                                (
                                                        String.format
                                                                (
                                                                        "/transacoes?conta=%s&agencia=%s",
                                                                        contaBancaria.getContaCorrente(),
                                                                        contaBancaria.getAgencia()
                                                                )
                                                )
                                        .withHeader("Content-Type", WireMock.equalTo(MediaType.APPLICATION_JSON_VALUE))
                                        .willReturn
                                                (
                                                        aResponse()
                                                                .withHeader("Content-type", MediaType.APPLICATION_JSON_VALUE)
                                                                .withBodyFile("json/transacoes.json")
                                                )
                        );

        final LinkedMultiValueMap<String, String> valueMap = new LinkedMultiValueMap<>();

        valueMap.add("agencia", contaBancaria.getAgencia());
        valueMap.add("conta", contaBancaria.getContaCorrente());
        valueMap.add("data_inicio", "01/01/2019");
        valueMap.add("data_fim", "01/01/2021");

        final String json = new String
                (
                        Files
                                .readAllBytes
                                        (
                                                ResourceUtils
                                                        .getFile("classpath:__files/json/consulta-transacoes.json")
                                                        .toPath()
                                        )
                );

        mockMvc
                .perform
                        (
                                get("/clientes/{cpf}/transactions", cliente.obterCpf())
                                        .params(valueMap)
                        )
                .andDo(print())
                .andExpect(header().stringValues("Content-Type", MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.transacoes.[*]").isArray())
                .andExpect(jsonPath("$.transacoes.[*]", hasSize(3)))
                .andExpect(content().json(json));

    }

}
