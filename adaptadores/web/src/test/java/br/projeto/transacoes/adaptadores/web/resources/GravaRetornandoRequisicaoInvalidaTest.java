package br.projeto.transacoes.adaptadores.web.resources;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.ResourceUtils;

import java.nio.file.Files;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@AutoConfigureWireMock(port = 0)
@ActiveProfiles("test")
class GravaRetornandoRequisicaoInvalidaTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    @DisplayName("Quando encaminhar cpf invalido deve responder status com erro")
    void quandoEncaminharCpfInvalidoDeveResponderStatusErro() throws Exception {

        final String jsonResquest = new String
                (
                        Files
                                .readAllBytes
                                        (
                                                ResourceUtils
                                                        .getFile("classpath:__files/json/gravar-transacoes-cpf-invalido.json")
                                                        .toPath()
                                        )
                );

        final String jsonResponse = new String
                (
                        Files
                                .readAllBytes
                                        (
                                                ResourceUtils
                                                        .getFile("classpath:__files/json/grava-cliente-invalido.json")
                                                        .toPath()
                                        )
                );
        mockMvc
                .perform
                        (
                                post("/clientes")
                                        .content
                                                (jsonResquest)
                                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        )
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(header().stringValues("Content-Type", MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*]").isArray())
                .andExpect(jsonPath("$.[*]", hasSize(4)))
                .andExpect(content().json(jsonResponse));

    }
}
