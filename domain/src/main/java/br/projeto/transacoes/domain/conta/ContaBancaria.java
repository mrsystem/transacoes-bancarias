package br.projeto.transacoes.domain.conta;

import java.util.Objects;
import java.util.Set;
import java.util.regex.Pattern;

public class ContaBancaria {

    private static final Pattern padraoAgencia = Pattern.compile("^\\d{4}$");

    public static boolean agenciaInvalida(final String agencia) {
        if (agencia == null) return true;
        return !padraoAgencia.matcher(agencia).matches();
    }

    private static final Pattern padraoContaCorrente = Pattern.compile("^\\d{2,5}-?\\d$");

    public static boolean contaCorrenteInvalida(final String contaCorrente) {
        if (contaCorrente == null) return true;
        return !padraoContaCorrente.matcher(contaCorrente).matches();
    }

    /**
     * @return coleção com apenas uma conta bancaria
     */
    public static Set<ContaBancaria> mockSet() {
        return Set.of(mock());
    }

    /**
     * @return conta bancaria com agencia = 0123 e conta corrente = 12345-3
     */
    public static ContaBancaria mock() {
        return builder
                (
                        builder()
                                .paraAgencia("0123")
                                .comContaCorrente("12345-3")
                                .build()
                )
                .build();
    }

    public static class ContaBancariaBuilder {

        private ContaBancariaBuilder(final ContaBancaria contaBancaria) {
            agencia = contaBancaria.getAgencia();
            contaCorrente = contaBancaria.getContaCorrente();
        }

        private ContaBancariaBuilder() {
        }

        private String agencia;

        /**
         * @param agencia cadeia que represente agência
         * @return mesma instância ContaBancariaBuilder
         * @throws IllegalArgumentException será lançada caso agência seja invalida
         */
        public ContaBancariaBuilder paraAgencia(final String agencia) {

            if (agenciaInvalida(agencia)) throw new IllegalArgumentException("agencia invalida");

            this.agencia = agencia;

            return this;

        }

        private String contaCorrente;

        /**
         * @param contaCorrente cadeia que represente uma conta corrente
         * @return mesma instância ContaBancariaBuilder
         * @throws IllegalArgumentException será lançada caso conta corrente não seja invalida
         */
        public ContaBancariaBuilder comContaCorrente(final String contaCorrente) {

            if (contaCorrenteInvalida(contaCorrente)) throw new IllegalArgumentException("conta invalida");

            this.contaCorrente = contaCorrente;

            return this;

        }

        public ContaBancaria build() {
            return new ContaBancaria(this);
        }

    }

    public static ContaBancariaBuilder builder() {
        return new ContaBancariaBuilder();
    }

    public static ContaBancariaBuilder builder(final ContaBancaria contaBancaria) {
        return new ContaBancariaBuilder(contaBancaria);
    }

    private ContaBancaria(final ContaBancariaBuilder contaBancariaBuilder) {
        this.contaBancariaBuilder = contaBancariaBuilder;
    }

    private final ContaBancariaBuilder contaBancariaBuilder;

    public String getAgencia() {
        return contaBancariaBuilder.agencia;
    }

    public String getContaCorrente() {
        return contaBancariaBuilder.contaCorrente;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContaBancaria that = (ContaBancaria) o;
        return Objects.equals(getAgencia(), that.getAgencia()) &&
                Objects.equals(getContaCorrente(), that.getContaCorrente());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAgencia(), getContaCorrente());
    }

}
