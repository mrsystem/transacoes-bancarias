package br.projeto.transacoes.domain.transacao;

public interface TransacaoGateway extends TransacaoQuery, TransacaoCommand {
}
