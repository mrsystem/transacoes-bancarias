package br.projeto.transacoes.domain.transacao;

import br.projeto.transacoes.domain.conta.ContaBancaria;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Set;
import java.util.UUID;

public class Transacao implements Comparable<Transacao> {

    public static boolean valorValido(final BigDecimal valor) {
        return valor.compareTo(BigDecimal.ZERO) > 0;
    }

    public static TransacaoBuilder builder(final LocalDateTime dataHoraEfetivacao) {
        return builder(dataHoraEfetivacao.toLocalDate(), dataHoraEfetivacao.toLocalTime());
    }

    public static TransacaoBuilder builder(final LocalDate dataEfetivacao, final LocalTime horaEfetivacao) {
        return new TransacaoBuilder(dataEfetivacao, horaEfetivacao);
    }

    public static TransacaoBuilder builder(final Transacao transacao) {
        return new TransacaoBuilder(transacao);
    }

    public static class TransacaoBuilder {

        private TransacaoBuilder(final LocalDate dataEfetiva, final LocalTime horaEfetiva) {
            this.dataEfetivacao = dataEfetiva;
            this.horaEfetivacao = horaEfetiva;
            autenticacao = UUID.randomUUID().toString();
        }

        private TransacaoBuilder(final Transacao transacao) {
            dataEfetivacao = transacao.obterDataEfetivacao();
            horaEfetivacao = transacao.obterHoraEfetivacao();
            autenticacao = transacao.obterAutenticacao();
            valor = transacao.obterValor();
            categoriaTransacao = transacao.obterCategoria();
            descricao = transacao.obterDescricao();
            contaBancaria = transacao.obterContaBancaria();
        }

        private final LocalDate dataEfetivacao;

        private final LocalTime horaEfetivacao;

        private String autenticacao;

        public TransacaoBuilder comAutenticacao(final String autenticacao) {
            if (autenticacao.length() > 5) this.autenticacao = autenticacao;
            return this;
        }

        private BigDecimal valor;

        /**
         * @param valor referente a transação
         * @return mesma instância de TransacaoBuilder
         * @throws IllegalArgumentException será lançada caso valor <= 0
         */
        public TransacaoBuilder comValor(final BigDecimal valor) {

            if (!valorValido(valor)) throw new IllegalArgumentException("Valor informado é ínvalido");

            this.valor = valor;

            return this;

        }

        private CategoriaTransacao categoriaTransacao;

        public TransacaoBuilder suaCategoria(final CategoriaTransacao categoriaTransacao) {
            this.categoriaTransacao = categoriaTransacao;
            return this;
        }

        private String descricao;

        public TransacaoBuilder comDescricao(final String descricao) {
            this.descricao = descricao;
            return this;
        }

        private ContaBancaria contaBancaria;

        public TransacaoBuilder paraContaBancaria(final ContaBancaria contaBancaria) {
            this.contaBancaria = contaBancaria;
            return this;
        }

        public Transacao build() {
            return new Transacao(this);
        }

    }

    /**
     * Para facilitar desenvolvimento do sistema, mockAll oferece uma coleção de transações bancárias
     *
     * @param transacao uma instância de transação base
     * @return Uma coleção de transações bancárias util para testar o dominio de transação
     */
    public static Set<Transacao> mockAll(final Transacao transacao) {

        final LocalDateTime now = transacao.obterDataHoraEfetivacao();

        return Set.of
                (
                        Transacao
                                .builder(transacao)
                                .suaCategoria(CategoriaTransacao.TAXA)
                                .build(),
                        Transacao
                                .builder(now.plusDays(1))
                                .paraContaBancaria(transacao.obterContaBancaria())
                                .comAutenticacao(transacao.obterAutenticacao())
                                .comDescricao(transacao.obterDescricao())
                                .suaCategoria(CategoriaTransacao.TAXA)
                                .comValor(BigDecimal.valueOf(132546))
                                .build(),
                        Transacao
                                .builder(now.minusDays(1))
                                .paraContaBancaria(transacao.obterContaBancaria())
                                .comAutenticacao(transacao.obterAutenticacao())
                                .comDescricao("Compra de teclado logitech")
                                .suaCategoria(CategoriaTransacao.COMPRA)
                                .comValor(BigDecimal.valueOf(500))
                                .build(),
                        Transacao
                                .builder(now.minusHours(5))
                                .paraContaBancaria(transacao.obterContaBancaria())
                                .comAutenticacao(transacao.obterAutenticacao())
                                .comDescricao("Compra de teclado corsair")
                                .suaCategoria(CategoriaTransacao.COMPRA)
                                .comValor(BigDecimal.valueOf(800))
                                .build(),
                        Transacao
                                .builder(now.minusHours(2))
                                .paraContaBancaria(transacao.obterContaBancaria())
                                .comAutenticacao(transacao.obterAutenticacao())
                                .comDescricao(transacao.obterDescricao())
                                .suaCategoria(CategoriaTransacao.TRANSFERENCIA)
                                .comValor(BigDecimal.valueOf(8000))
                                .build(),
                        Transacao
                                .builder(now.minusHours(1))
                                .paraContaBancaria(transacao.obterContaBancaria())
                                .comAutenticacao(transacao.obterAutenticacao())
                                .comDescricao(transacao.obterDescricao())
                                .suaCategoria(CategoriaTransacao.TRANSFERENCIA)
                                .comValor(BigDecimal.valueOf(5000))
                                .build(),
                        Transacao
                                .builder(now.minusDays(2))
                                .paraContaBancaria(transacao.obterContaBancaria())
                                .comAutenticacao(transacao.obterAutenticacao())
                                .comDescricao(transacao.obterDescricao())
                                .suaCategoria(CategoriaTransacao.TRANSFERENCIA)
                                .comValor(BigDecimal.valueOf(5000))
                                .build()
                );
    }

    /**
     * Transação bancária com data e hora atual
     *
     * @return uma instância mock de transação para testes
     */
    public static Transacao mock() {
        return mock(ContaBancaria.mock());
    }

    public static Transacao mock(final ContaBancaria contaBancaria) {
        return builder(mock(LocalDateTime.now()))
                .paraContaBancaria(contaBancaria)
                .build();
    }

    /**
     * Transação bancária com data e hora de efetivação como parametro de entrada
     *
     * @param dataHoraEfetivacao data e hora de efetivação de transação bancária
     * @return uma instância mock de transação para testes
     */
    public static Transacao mock(final LocalDateTime dataHoraEfetivacao) {
        return
                builder(dataHoraEfetivacao)
                        .comDescricao("Taxa bancária")
                        .comValor(BigDecimal.valueOf(120.34))
                        .suaCategoria(CategoriaTransacao.TAXA)
                        .build();
    }

    private Transacao(final TransacaoBuilder transacaoBuilder) {
        this.transacaoBuilder = transacaoBuilder;
    }

    private final TransacaoBuilder transacaoBuilder;

    @Override
    public int compareTo(final Transacao transacao) {
        return obterDataHoraEfetivacao().compareTo(transacao.obterDataHoraEfetivacao());
    }

    public boolean noIntervalo(final Intervalo intervalo) {
        return intervalo.dentro(obterDataHoraEfetivacao().toLocalDate());
    }

    public String obterAutenticacao() {
        return transacaoBuilder.autenticacao;
    }

    public BigDecimal obterValor() {
        return transacaoBuilder.valor;
    }

    public String obterDescricao() {
        return transacaoBuilder.descricao;
    }

    public CategoriaTransacao obterCategoria() {
        return transacaoBuilder.categoriaTransacao;
    }

    public LocalDate obterDataEfetivacao() {
        return transacaoBuilder.dataEfetivacao;
    }

    public LocalTime obterHoraEfetivacao() {
        return transacaoBuilder.horaEfetivacao;
    }

    public LocalDateTime obterDataHoraEfetivacao() {
        return LocalDateTime.of(transacaoBuilder.dataEfetivacao, transacaoBuilder.horaEfetivacao);
    }

    public ContaBancaria obterContaBancaria() {
        return transacaoBuilder.contaBancaria;
    }

}
