package br.projeto.transacoes.domain.transacao;

import br.projeto.transacoes.domain.conta.ContaBancaria;

import java.util.Set;

@FunctionalInterface
public interface TransacaoQuery {

    static TransacaoQuery mock(final Transacao transacao) {
        return contaBancaria -> Transacao.mockAll(transacao);
    }

    Set<Transacao> listar(final ContaBancaria contaBancaria);

}
