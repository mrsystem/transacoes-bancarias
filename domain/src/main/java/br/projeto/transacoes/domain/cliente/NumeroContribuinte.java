package br.projeto.transacoes.domain.cliente;

import java.util.function.Predicate;
import java.util.regex.Pattern;

abstract class NumeroContribuinte implements Predicate<String> {

    private NumeroContribuinte
            (
                    final int tamanhoCadeia,
                    final int basePrimeiroDigito,
                    final int baseSegundoDigito
            ) {
        this.tamanhoCadeia = tamanhoCadeia;
        this.basePrimeiroDigito = basePrimeiroDigito;
        this.baseSegundoDigito = baseSegundoDigito;
    }

    private final int tamanhoCadeia;
    private final int basePrimeiroDigito;
    private final int baseSegundoDigito;

    @Override
    public boolean test(final String cadeiaNumeroContribuinte) {

        if (cadeiaNumeroContribuinte == null) return false;

        if (!matchExpressaoRegular(cadeiaNumeroContribuinte)) return false;

        final int[] numeroContribuinte = CalculoModulo11.transformarCadeiaDeCaracteresEmUmArray(cadeiaNumeroContribuinte);

        if (digitosIguais(numeroContribuinte)) return false;

        return digitosVerificadoresValidos(numeroContribuinte);

    }

    private boolean digitosVerificadoresValidos(final int[] numeroContribuinte) {

        final int primeiroDigito = CalculoModulo11.calcularDigitoVerificador(numeroContribuinte, basePrimeiroDigito, tamanhoCadeia - 2);

        if (primeiroDigitoVerificadorIncorreto(numeroContribuinte, primeiroDigito)) return false;

        final int segundoDigito = CalculoModulo11.calcularDigitoVerificador(numeroContribuinte, baseSegundoDigito, tamanhoCadeia - 1);

        return segundoDigitoVerificadorCorreto(numeroContribuinte, segundoDigito);

    }

    private boolean digitosIguais(int[] digitos) {

        int i = 1;

        while (i < tamanhoCadeia) {

            if (digitos[0] != digitos[i++]) return false;

        }

        return true;

    }

    private boolean primeiroDigitoVerificadorIncorreto(final int[] numeroContribuinte, final int digitoVerificador) {

        final int indice = indiceDoPrimeiroDigitoVerificador();

        return CalculoModulo11.digitoVerificadorIncorreto(indice, numeroContribuinte, digitoVerificador);

    }

    private boolean segundoDigitoVerificadorCorreto(final int[] numeroContribuinte, final int digitoVerificador) {

        final int indice = indiceDoSegundoDigitoVerificador();

        return !CalculoModulo11.digitoVerificadorIncorreto(indice, numeroContribuinte, digitoVerificador);

    }

    /**
     * @param numeroContribuinte referente a cadeia de caracteres do numero do contribuinte
     * @return caso ocorra um match com expressao regular, retornara true
     */
    protected abstract boolean matchExpressaoRegular(final String numeroContribuinte);

    /**
     * Hook Method
     *
     * @return indice do primeiro digito é necessário para pesquisar no array o valor referente ao digito
     * verificador
     */
    protected abstract int indiceDoPrimeiroDigitoVerificador();

    /**
     * Hook Method
     *
     * @return indice do segundo digito é necessário para pesquisar no array o valor referente ao digito
     * verificador
     */
    protected abstract int indiceDoSegundoDigitoVerificador();

    static class PredicadoCPF extends NumeroContribuinte {

        private static final Pattern EXPRESSAO_REGULAR_CPF =
                Pattern.compile("^\\d{3}\\.?\\d{3}\\.?\\d{3}-?\\d{2}$", Pattern.CASE_INSENSITIVE);

        PredicadoCPF() {
            super(11, 11, 12);
        }

        @Override
        protected boolean matchExpressaoRegular(final String numeroContribuinte) {
            return EXPRESSAO_REGULAR_CPF.matcher(numeroContribuinte).matches();
        }

        @Override
        protected int indiceDoPrimeiroDigitoVerificador() {
            return 10;
        }

        @Override
        protected int indiceDoSegundoDigitoVerificador() {
            return 11;
        }

    }

}
