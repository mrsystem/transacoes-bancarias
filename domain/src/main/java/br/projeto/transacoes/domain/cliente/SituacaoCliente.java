package br.projeto.transacoes.domain.cliente;

import java.util.stream.Stream;

public enum SituacaoCliente {

    PENDENTE(0),
    ATIVADO(1),
    CANCELADO(2),
    OUTRO(3);

    public static SituacaoCliente valueOf(final int codigo) {

        return Stream
                .of(values())
                .filter(situacaoCliente -> situacaoCliente.isEqual(codigo))
                .findFirst()
                .orElse(OUTRO);

    }

    SituacaoCliente(final int codigo) {
        this.codigo = codigo;
    }

    private final int codigo;

    public int toInt() {
        return codigo;
    }

    private boolean isEqual(final int codigo) {
        return toInt() == codigo;
    }

}
