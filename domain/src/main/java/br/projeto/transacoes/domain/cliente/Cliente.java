package br.projeto.transacoes.domain.cliente;

import br.projeto.transacoes.domain.conta.ContaBancaria;

import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;

public class Cliente {

    private static final Predicate<String> PREDICADO_CPF = new NumeroContribuinte.PredicadoCPF();

    public static boolean cpfValido(final String cpf) {
        return PREDICADO_CPF.test(limparValorCpf(cpf));
    }

    private static String limparValorCpf(final String cpf) {
        return Optional
                .ofNullable(cpf)
                .orElse("")
                .replaceAll("\\D", "");
    }

    public static ClienteBuilder builder() {
        return new ClienteBuilder();
    }

    public static ClienteBuilder builder(final Cliente cliente) {
        return new ClienteBuilder(cliente);
    }

    public static class ClienteBuilder {

        private ClienteBuilder() {
            contasBancaria = Set.of();
            situacaoCliente = SituacaoCliente.OUTRO;
            cpf = "";
        }

        private ClienteBuilder(final Cliente cliente) {
            contasBancaria = cliente.obterContasBancaria();
            situacaoCliente = cliente.obterSituacao();
            cpf = cliente.obterCpf();
        }

        private String cpf;

        /**
         * @param cpf cadeia de caracteres que represente um cadastro pessoa física
         * @return mesma instância de ClienteBuilder
         * @throws IllegalArgumentException será lançada caso cpf seja inválido
         */
        public ClienteBuilder comCpf(final String cpf) {

            if (!cpfValido(cpf)) throw new IllegalArgumentException("Cpf invalido");

            this.cpf = limparValorCpf(cpf);

            return this;

        }

        private SituacaoCliente situacaoCliente;

        public ClienteBuilder situacao(final SituacaoCliente situacaoCliente) {
            this.situacaoCliente = situacaoCliente;
            return this;
        }

        private Set<ContaBancaria> contasBancaria;

        public ClienteBuilder possuiContas(final Set<ContaBancaria> contasBancaria) {
            this.contasBancaria = contasBancaria;
            return this;
        }

        public Cliente build() {
            return new Cliente(this);
        }

    }

    /**
     * @return cliente com cpf valido, situacao igual a outro e com uma coleção de contas bancárias de tamanho 1
     */
    public static Cliente mock() {
        return builder
                (
                        builder()
                                .comCpf("349.688.890-38")
                                .situacao(SituacaoCliente.OUTRO)
                                .possuiContas(ContaBancaria.mockSet())
                                .build()
                ).build();
    }

    private Cliente(final ClienteBuilder clienteBuilder) {
        this.clienteBuilder = clienteBuilder;
    }

    private final ClienteBuilder clienteBuilder;

    public boolean habilitado() {
        return obterSituacao().equals(SituacaoCliente.ATIVADO);
    }

    public boolean identificadoPeloCpf(final String cpf) {
        return obterCpf().equals(cpf);
    }

    public Optional<ContaBancaria> contaBancaria(final String agencia, final String conta) {

        final ContaBancaria contaBancaria =
                ContaBancaria
                        .builder()
                        .paraAgencia(agencia)
                        .comContaCorrente(conta)
                        .build();

        return obterContasBancaria()
                .stream()
                .filter(contaBancaria::equals)
                .findFirst();

    }

    public String obterCpf() {
        return clienteBuilder.cpf;
    }

    public SituacaoCliente obterSituacao() {
        return clienteBuilder.situacaoCliente;
    }

    public Set<ContaBancaria> obterContasBancaria() {
        return clienteBuilder.contasBancaria;
    }

}
