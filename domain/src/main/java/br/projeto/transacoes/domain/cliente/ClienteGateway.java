package br.projeto.transacoes.domain.cliente;

public interface ClienteGateway extends ClienteQuery, ClienteCommand {
}
