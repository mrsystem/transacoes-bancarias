package br.projeto.transacoes.domain.cliente;

@FunctionalInterface
public interface ClienteCommand {

    void salvar(final Cliente cliente);

}
