package br.projeto.transacoes.domain.transacao;

import java.util.stream.Stream;

public enum CategoriaTransacao {

    TAXA(10),
    COMPRA(50),
    TRANSFERENCIA(75),
    OUTRO(100);

    public static CategoriaTransacao valueOf(final int codigo) {
        return Stream
                .of(values())
                .filter(categoriaTransacao -> categoriaTransacao.toInt() == codigo)
                .findFirst()
                .orElse(OUTRO);
    }

    CategoriaTransacao(final int codigo) {
        this.codigo = codigo;
    }

    private final int codigo;

    public int toInt() {
        return codigo;
    }

}
