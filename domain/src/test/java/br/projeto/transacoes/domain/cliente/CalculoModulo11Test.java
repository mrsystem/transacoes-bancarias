package br.projeto.transacoes.domain.cliente;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class CalculoModulo11Test {

    @Test
    void transformarCadeiaCaracteresEmArrayInt() {

        final int[] numeraisEsperados = {2, 1, 9, 7, 8, 6, 8, 5, 0, 1, 9};

        final int[] numeraisRecebido = CalculoModulo11.transformarCadeiaDeCaracteresEmUmArray("219.786.850-19");

        Assertions.assertArrayEquals(numeraisEsperados, numeraisRecebido);

    }

    @Test
    void transformarCadeiaCaracteresVaziaEmArrayInt() {

        final int[] numeraisRecebido = CalculoModulo11.transformarCadeiaDeCaracteresEmUmArray("");

        Assertions.assertEquals(0, numeraisRecebido.length);

    }

    @Test
    void transformarCadeiaCaracteresNaoNumericaEmArrayInt() {

        final int[] numeraisRecebido = CalculoModulo11.transformarCadeiaDeCaracteresEmUmArray(".....adasdsasddijijauhuf-");

        Assertions.assertEquals(0, numeraisRecebido.length);

    }

    @Test
    void digitoVerificadorIncorreto() {

        final int[] numerais = {2, 1, 9, 7, 8, 6, 8, 5, 0, 1, 9};

        Assertions.assertTrue(CalculoModulo11.digitoVerificadorIncorreto(1, numerais, 8));

    }

    @Test
    void digitoVerificadorCorreto() {

        final int[] numerais = {2, 1, 9, 7, 8, 6, 8, 5, 0, 1, 9};

        Assertions.assertFalse(CalculoModulo11.digitoVerificadorIncorreto(1, numerais, 2));

    }

    @Test
    void calcularDigitoVerificadorIncorreto() {

        final int[] numerais = {2, 1, 9, 7, 8, 6, 8, 5, 0, 1, 9};

        final int digitoVerificador = CalculoModulo11.calcularDigitoVerificador(numerais, 11, 9);

        Assertions.assertTrue(CalculoModulo11.digitoVerificadorIncorreto(10, numerais, digitoVerificador));

    }

    @Test
    void calcularDigitoVerificadorComValorZeroCorreto() {

        final int[] numerais = {2, 1, 9, 7, 8, 6, 8, 5, 0, 0, 9};

        final int digitoVerificador = CalculoModulo11.calcularDigitoVerificador(numerais, 11, 9);

        Assertions.assertFalse(CalculoModulo11.digitoVerificadorIncorreto(10, numerais, digitoVerificador));

    }

    @Test
    void calcularDigitoVerificadorComValorTresCorreto() {

        final int[] numerais = {7, 3, 5, 9, 5, 9, 5, 7, 0, 3, 2};

        final int digitoVerificador = CalculoModulo11.calcularDigitoVerificador(numerais, 11, 9);

        Assertions.assertFalse(CalculoModulo11.digitoVerificadorIncorreto(10, numerais, digitoVerificador));

    }

}