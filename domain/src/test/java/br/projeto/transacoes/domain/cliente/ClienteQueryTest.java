package br.projeto.transacoes.domain.cliente;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Optional;

class ClienteQueryTest {

    @Test
    void deveRetornarUmCliente() {

        final Cliente cliente = Cliente.mock();

        final Optional<Cliente> clienteOptional = ClienteQuery.mock(cliente).recuperarPorCpf(cliente.obterCpf());

        Assertions.assertTrue(clienteOptional.isPresent());

        final Cliente clienteRecuperado = clienteOptional.get();

        Assertions.assertEquals(cliente, clienteRecuperado);

    }

    @Test
    void deveRetornarOptionalVazio() {

        final Cliente cliente = Cliente.mock();

        final Optional<Cliente> clienteOptional = ClienteQuery.mock(cliente).recuperarPorCpf("");

        Assertions.assertTrue(clienteOptional.isEmpty());

    }

}