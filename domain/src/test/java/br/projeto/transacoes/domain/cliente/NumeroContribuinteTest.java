package br.projeto.transacoes.domain.cliente;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.function.Predicate;

class NumeroContribuinteTest {

    final Predicate<String> predicado = new NumeroContribuinte.PredicadoCPF();

    @Test
    void cenarioCadeiaComPrimeiroDigitoVerificadorErrado() {

        Assertions.assertFalse(predicado.test("219.786.850-19"));

    }

    @Test
    void cenarioCadeiaComSegundoDigitoVerificadorErrado() {

        Assertions.assertFalse(predicado.test("219.786.850-08"));

    }

    @Test
    void cenarioCPFValido() {

        Assertions.assertTrue(predicado.test("219.786.850-09"));
        Assertions.assertTrue(predicado.test("73595957032"));

    }

    @Test
    void contribuinteTipoCPFInvalidos() {

        Assertions.assertFalse(predicado.test(null));
        Assertions.assertFalse(predicado.test(""));
        Assertions.assertFalse(predicado.test("735959570321"));
        Assertions.assertFalse(predicado.test("000.000.000-00"));
        Assertions.assertFalse(predicado.test("111.111.111-11"));
        Assertions.assertFalse(predicado.test("222.222.222-22"));
        Assertions.assertFalse(predicado.test("333.333.333-33"));
        Assertions.assertFalse(predicado.test("444.444.444-44"));
        Assertions.assertFalse(predicado.test("55555555555"));
        Assertions.assertFalse(predicado.test("666.666.666-66"));
        Assertions.assertFalse(predicado.test("777.777.777-77"));
        Assertions.assertFalse(predicado.test("888.888.888-88"));
        Assertions.assertFalse(predicado.test("999.999.999-99"));

    }

}