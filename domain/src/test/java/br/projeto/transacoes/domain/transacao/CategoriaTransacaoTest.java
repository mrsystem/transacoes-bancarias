package br.projeto.transacoes.domain.transacao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class CategoriaTransacaoTest {

    @Test
    void deveRetornarCategoriaTaxa() {

        final int codigo = 10;

        final CategoriaTransacao taxa = CategoriaTransacao.valueOf(codigo);

        Assertions.assertEquals(CategoriaTransacao.TAXA, taxa);

        Assertions.assertEquals(codigo, taxa.toInt());

    }

    @Test
    void deveRetornarCategoriaCompra() {

        final int codigo = 50;

        final CategoriaTransacao compra = CategoriaTransacao.valueOf(codigo);

        Assertions.assertEquals(CategoriaTransacao.COMPRA, compra);

        Assertions.assertEquals(codigo, compra.toInt());

    }

    @Test
    void deveRetornarCategoriaTransferencia() {

        final int codigo = 75;

        final CategoriaTransacao transferencia = CategoriaTransacao.valueOf(codigo);

        Assertions.assertEquals(CategoriaTransacao.TRANSFERENCIA, transferencia);

        Assertions.assertEquals(codigo, transferencia.toInt());

    }

    @Test
    void deveRetornarCategoriaOutro() {

        final CategoriaTransacao transferencia = CategoriaTransacao.valueOf(-1);

        Assertions.assertEquals(CategoriaTransacao.OUTRO, transferencia);

        Assertions.assertEquals(100, transferencia.toInt());

    }

}