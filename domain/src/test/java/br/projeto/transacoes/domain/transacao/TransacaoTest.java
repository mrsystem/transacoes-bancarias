package br.projeto.transacoes.domain.transacao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;

class TransacaoTest {

    @Test
    void deveConstruirTransacaoComDataHoraEfetivacaoAtual() {

        final LocalDateTime dataHoraEfetivacao = LocalDateTime.now();

        final Transacao transacao = Transacao.mock(dataHoraEfetivacao);

        Assertions.assertEquals(dataHoraEfetivacao.toLocalDate(), transacao.obterDataEfetivacao());

        Assertions.assertEquals(dataHoraEfetivacao.toLocalTime(), transacao.obterHoraEfetivacao());

        Assertions.assertEquals(dataHoraEfetivacao, transacao.obterDataHoraEfetivacao());

    }

    @Test
    void dataDeveEstarNoIntervalo() {

        final LocalDateTime dataHoraEfetivacao = LocalDateTime.now();

        final Transacao transacao = Transacao.mock(dataHoraEfetivacao);

        Assertions.assertTrue(transacao.noIntervalo(Intervalo.mock(dataHoraEfetivacao.toLocalDate())));

    }

    @Test
    void transacaoDeveSerCategoriaTaxa() {

        final Transacao transacao = Transacao.mock();

        Assertions.assertEquals(CategoriaTransacao.TAXA, transacao.obterCategoria());

    }

    @Test
    void transacoesComDataHoraEfetivacaoIgual() {

        final LocalDateTime dataHoraEfetivacao = LocalDateTime.now();

        final Transacao transacao = Transacao.mock(dataHoraEfetivacao);

        Assertions.assertEquals(0, transacao.compareTo(Transacao.mock(dataHoraEfetivacao)));

    }

    @Test
    void transacaoComDataAnterior() {

        final LocalDateTime dataHoraEfetivacao = LocalDateTime.now();

        final Transacao transacao = Transacao.mock(dataHoraEfetivacao);

        Assertions.assertEquals(-1, transacao.compareTo(Transacao.mock(dataHoraEfetivacao.plusDays(1))));

    }

    @Test
    void transacaoComDataPosterior() {

        final LocalDateTime dataHoraEfetivacao = LocalDateTime.now();

        final Transacao transacao = Transacao.mock(dataHoraEfetivacao);

        Assertions.assertEquals(1, transacao.compareTo(Transacao.mock(dataHoraEfetivacao.minusDays(1))));

    }

    @Test
    void valorValido(){

        Assertions
                .assertTrue(Transacao.valorValido(BigDecimal.ONE));

    }

    @Test
    void valorInvalido(){

        Assertions
                .assertFalse(Transacao.valorValido(BigDecimal.ZERO));

    }

    @Test
    void transacaoComValorMenorQueZero() {

        Assertions.assertThrows
                (
                        IllegalArgumentException.class,
                        () -> Transacao
                                .builder(LocalDateTime.now())
                                .comValor(BigDecimal.valueOf(-1))
                );

    }

    @Test
    void transacaoComAutenticacaoComTamanhoMenorQueCinco() {

        final String autenticacao = "1234";

        final Transacao transacao =
                Transacao
                        .builder(LocalDateTime.now())
                        .comAutenticacao(autenticacao)
                        .build();

        Assertions.assertNotNull(transacao.obterAutenticacao());

        Assertions.assertNotEquals(autenticacao, transacao.obterAutenticacao());

    }

    @Test
    void transacaoComAutenticacaoComTamanhoMaiorQueCinco() {

        final String autenticacao = "123456";

        final Transacao transacao =
                Transacao
                        .builder(LocalDateTime.now())
                        .comAutenticacao(autenticacao)
                        .build();

        Assertions.assertNotNull(transacao.obterAutenticacao());

        Assertions.assertEquals(autenticacao, transacao.obterAutenticacao());

    }
}