package br.projeto.transacoes.domain.cliente;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class SituacaoClienteTest {

    @Test
    void deveRetornarSituacoPendente() {

        final int codigo = 0;

        final SituacaoCliente pendente = SituacaoCliente.valueOf(codigo);

        Assertions.assertEquals(SituacaoCliente.PENDENTE, pendente);

        Assertions.assertEquals(codigo, pendente.toInt());

    }

    @Test
    void deveRetornarSituacoAtivado() {

        final int codigo = 1;

        final SituacaoCliente pendente = SituacaoCliente.valueOf(codigo);

        Assertions.assertEquals(SituacaoCliente.ATIVADO, pendente);

        Assertions.assertEquals(codigo, pendente.toInt());

    }

    @Test
    void deveRetornarSituacoCancelado() {

        final int codigo = 2;

        final SituacaoCliente pendente = SituacaoCliente.valueOf(codigo);

        Assertions.assertEquals(SituacaoCliente.CANCELADO, pendente);

        Assertions.assertEquals(codigo, pendente.toInt());

    }

    @Test
    void deveRetornarSituacoOutro() {

        final SituacaoCliente pendente = SituacaoCliente.valueOf(-1);

        Assertions.assertEquals(SituacaoCliente.OUTRO, pendente);

        Assertions.assertEquals(3, pendente.toInt());

    }

}