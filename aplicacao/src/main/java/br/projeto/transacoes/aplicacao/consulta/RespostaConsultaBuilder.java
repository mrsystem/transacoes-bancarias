package br.projeto.transacoes.aplicacao.consulta;

import br.projeto.transacoes.aplicacao.resposta.Erro;
import br.projeto.transacoes.domain.transacao.CategoriaTransacao;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

class RespostaConsultaBuilder {

    static RespostaConsulta valueOf(final Set<Erro> erros, int status) {
        return
                builder()
                        .comStatus(status)
                        .comColecaoDeErros(erros)
                        .build();
    }

    static RespostaConsultaBuilder builder() {
        return new RespostaConsultaBuilder();
    }

    private RespostaConsultaBuilder() {
        erros = new LinkedHashSet<>();
        transacoes = new HashMap<>();
    }

    private int status;

    RespostaConsultaBuilder comStatus(final int status) {
        this.status = status;
        return this;
    }

    private final Set<Erro> erros;

    RespostaConsultaBuilder comColecaoDeErros(final Set<Erro> erros) {
        this.erros.clear();
        this.transacoes.clear();
        this.erros.addAll(erros);
        return this;
    }

    RespostaConsultaBuilder comErro(final Erro erro) {
        this.transacoes.clear();
        erros.add(erro);
        return this;
    }

    private final Map<CategoriaTransacao, Set<RespostaTransacao>> transacoes;

    RespostaConsultaBuilder paraTransacoes(final Map<CategoriaTransacao, ? extends Set<RespostaTransacao>> transacoes) {
        this.transacoes.clear();
        this.erros.clear();
        this.transacoes.putAll(transacoes);
        return this;
    }

    RespostaConsulta build() {
        return new RespostaConsultaImplementacao(this);
    }

    private static class RespostaConsultaImplementacao implements RespostaConsulta {

        private RespostaConsultaImplementacao(final RespostaConsultaBuilder respostaConsultaBuilder) {
            this.respostaConsultaBuilder = respostaConsultaBuilder;
        }

        private final RespostaConsultaBuilder respostaConsultaBuilder;

        @Override
        public int obterStatus() {
            return respostaConsultaBuilder.status;
        }

        @Override
        public Set<Erro> obterErros() {
            return respostaConsultaBuilder.erros;
        }

        @Override
        public Map<CategoriaTransacao, Set<RespostaTransacao>> obterTransacoes() {
            return respostaConsultaBuilder.transacoes;
        }

    }

}
