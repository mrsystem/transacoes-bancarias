package br.projeto.transacoes.aplicacao.resposta;

final class ErroBuilderImplementacao implements ErroBuilder {

    ErroBuilderImplementacao() {
    }

    private String valor;

    @Override
    public ErroBuilder comValor(final String valor) {
        this.valor = valor;
        return this;
    }

    private TabelaCampo tabela;

    @Override
    public ErroBuilder campo(final TabelaCampo tabelaCampo) {
        tabela = tabelaCampo;
        return this;
    }

    private TipoErro tipoErro;

    @Override
    public ErroBuilder tipoErro(final TipoErro tipoErro) {
        this.tipoErro = tipoErro;
        return this;
    }

    @Override
    public Erro build() {
        return new ErroImplementacao(this);
    }

    private static class ErroImplementacao implements Erro {

        private ErroImplementacao(final ErroBuilderImplementacao erroBuilder) {
            this.erroBuilder = erroBuilder;
        }

        private final ErroBuilderImplementacao erroBuilder;

        @Override
        public String obterValor() {
            return erroBuilder.valor;
        }

        @Override
        public String obterCampo() {
            return erroBuilder.tabela.toString();
        }

        @Override
        public String obterMensagem() {
            return erroBuilder.tipoErro.toString();
        }

        @Override
        public int obterCodigoErro() {
            return erroBuilder.tipoErro.toInt();
        }

    }

}
