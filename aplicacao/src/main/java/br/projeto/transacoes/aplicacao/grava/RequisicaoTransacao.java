package br.projeto.transacoes.aplicacao.grava;

import br.projeto.transacoes.domain.transacao.CategoriaTransacao;
import br.projeto.transacoes.domain.transacao.Transacao;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public interface RequisicaoTransacao {

    class RequisicaoTransacaoSkeletal implements RequisicaoTransacao {

        RequisicaoTransacaoSkeletal(final Transacao transacao) {
            this.transacao = transacao;
        }

        private final Transacao transacao;

        @Override
        public LocalDateTime obterDataHoraEfetivacao() {
            return transacao.obterDataHoraEfetivacao();
        }

        @Override
        public String obterAutenticacao() {
            return transacao.obterAutenticacao();
        }

        @Override
        public BigDecimal obterValor() {
            return transacao.obterValor();
        }

        @Override
        public int obterCategoriaTransacao() {
            return transacao.obterCategoria().toInt();
        }

        @Override
        public String obterDescricao() {
            return transacao.obterDescricao();
        }

    }

    static RequisicaoTransacao mock(final Transacao transacao) {
        return new RequisicaoTransacaoSkeletal(transacao);
    }

    LocalDateTime obterDataHoraEfetivacao();

    String obterAutenticacao();

    BigDecimal obterValor();

    int obterCategoriaTransacao();

    String obterDescricao();

    default Transacao transacao(final RequisicaoContaBancaria requisicaoContaBancaria) {
        return Transacao
                .builder(obterDataHoraEfetivacao())
                .comAutenticacao(obterAutenticacao())
                .comValor(obterValor())
                .suaCategoria
                        (
                                CategoriaTransacao
                                        .valueOf(obterCategoriaTransacao())
                        )
                .comDescricao(obterDescricao())
                .paraContaBancaria(requisicaoContaBancaria.contaBancaria())
                .build();
    }
}
