package br.projeto.transacoes.aplicacao.resposta;

public interface TabelaStatus {

    class SkeletalTabelaStatus implements TabelaStatus {

        @Override
        public int statusRequestInvalid() {
            return 0;
        }

        @Override
        public int statusCustomerNotFound() {
            return 1;
        }

        @Override
        public int statusCustomerNotAuthorized() {
            return 2;
        }

        @Override
        public int statusBalanceAccountNotFound() {
            return 3;
        }

        @Override
        public int statusTransactionNotFound() {
            return 4;
        }

        @Override
        public int statusSuccess() {
            return 5;
        }

        @Override
        public int statusInternalError() {
            return 6;
        }

    }

    static TabelaStatus mock() {
        return new SkeletalTabelaStatus();
    }

    int statusRequestInvalid();

    int statusCustomerNotFound();

    int statusCustomerNotAuthorized();

    int statusBalanceAccountNotFound();

    int statusTransactionNotFound();

    int statusSuccess();

    int statusInternalError();
}
