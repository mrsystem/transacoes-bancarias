package br.projeto.transacoes.aplicacao.consulta;

import br.projeto.transacoes.domain.cliente.Cliente;
import br.projeto.transacoes.domain.cliente.ClienteQuery;
import br.projeto.transacoes.domain.conta.ContaBancaria;
import br.projeto.transacoes.domain.data.Dates;
import br.projeto.transacoes.domain.transacao.Intervalo;
import br.projeto.transacoes.domain.transacao.Transacao;

import java.time.LocalDate;
import java.util.Optional;

public interface RequisicaoConsulta {

    class SkeletalRequisicaoConsulta implements RequisicaoConsulta {

        SkeletalRequisicaoConsulta(final Cliente cliente, final LocalDate localDate) {

            this.cliente = cliente;

            contaBancaria = cliente.obterContasBancaria().iterator().next();

            dataInicio = Dates.format(localDate);

            dataFim = Dates.format(localDate.plusDays(2));

        }

        private final Cliente cliente;

        private final ContaBancaria contaBancaria;

        private final String dataInicio;

        private final String dataFim;

        @Override
        public String obterCpf() {
            return cliente.obterCpf();
        }

        @Override
        public String obterAgencia() {
            return contaBancaria.getAgencia();
        }

        @Override
        public String obterContaCorrente() {
            return contaBancaria.getContaCorrente();
        }

        @Override
        public String obterDataInicio() {
            return dataInicio;
        }

        @Override
        public String obterDataFim() {
            return dataFim;
        }

    }

    static RequisicaoConsulta mock(final Cliente cliente, final LocalDate now) {
        return new SkeletalRequisicaoConsulta(cliente, now);
    }

    String obterCpf();

    String obterAgencia();

    String obterContaCorrente();

    String obterDataInicio();

    String obterDataFim();

    default String obterDataInicioFim() {
        return Intervalo.formatarIntervalo(obterDataInicio(), obterDataFim());
    }

    default boolean noIntervalo(final Transacao transacao) {
        return transacao.noIntervalo(intervalo());
    }

    private Intervalo intervalo() {
        return Intervalo.between(obterDataInicio(), obterDataFim());
    }

    default Optional<Cliente> cliente(final ClienteQuery clienteQuery) {
        return clienteQuery.recuperarPorCpf(obterCpf());
    }

    default Optional<ContaBancaria> contaBancaria(final Cliente cliente) {
        return cliente.contaBancaria(obterAgencia(), obterContaCorrente());
    }

}
