package br.projeto.transacoes.aplicacao.consulta;

import br.project.knin.activity.Activity;
import br.projeto.transacoes.aplicacao.resposta.Erro;
import br.projeto.transacoes.aplicacao.resposta.TabelaCampo;
import br.projeto.transacoes.aplicacao.resposta.TabelaStatus;
import br.projeto.transacoes.aplicacao.resposta.TipoErro;
import br.projeto.transacoes.domain.cliente.Cliente;
import br.projeto.transacoes.domain.cliente.ClienteQuery;
import br.projeto.transacoes.domain.transacao.Transacao;
import br.projeto.transacoes.domain.transacao.TransacaoQuery;

import java.util.Optional;
import java.util.TreeSet;
import java.util.stream.Collectors;

final class CasoDeUsoApresentarTransacoesBancariaImpl implements CasoDeUsoApresentarTransacoesBancaria {

    CasoDeUsoApresentarTransacoesBancariaImpl
            (
                    final ClienteQuery clienteQuery,
                    final TransacaoQuery transacaoQuery,
                    final TabelaStatus tabelaStatus
            ) {
        this.clienteQuery = clienteQuery;
        this.transacaoQuery = transacaoQuery;
        this.tabelaStatus = tabelaStatus;
    }

    private final ClienteQuery clienteQuery;

    private final TransacaoQuery transacaoQuery;

    private final TabelaStatus tabelaStatus;

    @Override
    public RespostaConsulta consultarTransacoes(final RequisicaoConsulta requisicaoConsulta) {

        return Activity

                .contract(RespostaConsulta.nullable(tabelaStatus.statusInternalError()))

                .entry(requisicaoConsulta)

                .action(req -> req.cliente(clienteQuery))

                .decision(Optional::isPresent)

                .otherwise(req ->
                        RespostaConsultaBuilder
                                .builder()
                                .comErro
                                        (
                                                Erro
                                                        .builder()
                                                        .campo(TabelaCampo.CPF)
                                                        .comValor(req.obterCpf())
                                                        .tipoErro(TipoErro.CLIENTE_NAO_ENCONTRADO)
                                                        .build()
                                        )
                                .comStatus(tabelaStatus.statusCustomerNotFound())
                                .build()
                )

                .action(Optional::get)

                .decision(Cliente::habilitado)

                .otherwise(req ->
                        RespostaConsultaBuilder
                                .builder()
                                .comErro
                                        (
                                                Erro
                                                        .builder()
                                                        .campo(TabelaCampo.CPF)
                                                        .comValor(req.obterCpf())
                                                        .tipoErro(TipoErro.CLIENTE_NAO_AUTORIZADO)
                                                        .build()
                                        )
                                .comStatus(tabelaStatus.statusCustomerNotAuthorized())
                                .build()
                )

                .action(RequisicaoConsulta::contaBancaria)

                .decision(Optional::isPresent)

                .otherwise(req ->
                        RespostaConsultaBuilder
                                .builder()
                                .comErro
                                        (
                                                Erro
                                                        .builder()
                                                        .campo(TabelaCampo.CONTA_CORRENTE)
                                                        .comValor(req.obterCpf())
                                                        .tipoErro(TipoErro.CONTA_BANCARIA_NAO_ENCONTRADA)
                                                        .build()
                                        )
                                .comStatus(tabelaStatus.statusBalanceAccountNotFound())
                                .build()
                )

                .action(Optional::get)

                .action(transacaoQuery::listar)

                .action((req, transactions) ->
                        transactions
                                .parallelStream()
                                .filter(req::noIntervalo)
                                .collect
                                        (
                                                Collectors
                                                        .groupingByConcurrent
                                                                (
                                                                        Transacao::obterCategoria,
                                                                        Collectors.mapping
                                                                                (
                                                                                        RespostaTransacaoAdapterComparavel::valueOf,
                                                                                        Collectors.toCollection(TreeSet::new)
                                                                                )

                                                                )
                                        )
                )

                .decision(transactions -> !transactions.isEmpty())

                .otherwise(req ->
                        RespostaConsultaBuilder
                                .builder()
                                .comErro
                                        (
                                                Erro
                                                        .builder()
                                                        .campo(TabelaCampo.DATA_INICIO)
                                                        .comValor(req.obterCpf())
                                                        .tipoErro(TipoErro.PERIODO_SEM_TRANSACOES)
                                                        .build()
                                        )
                                .comStatus(tabelaStatus.statusTransactionNotFound())
                                .build()
                )

                .exit
                        (
                                transactionsMap ->
                                        RespostaConsultaBuilder
                                                .builder()
                                                .comStatus(tabelaStatus.statusSuccess())
                                                .paraTransacoes(transactionsMap)
                                                .build()
                        );

    }

}
