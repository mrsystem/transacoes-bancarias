package br.projeto.transacoes.aplicacao.resposta;

public interface Erro {

    static ErroBuilder builder() {
        return new ErroBuilderImplementacao();
    }

    static Erro mock() {
        return
                builder()
                        .tipoErro(TipoErro.INVALIDO)
                        .campo(TabelaCampo.CPF)
                        .comValor("123")
                        .build();
    }

    String obterValor();

    String obterCampo();

    String obterMensagem();

    int obterCodigoErro();

}
