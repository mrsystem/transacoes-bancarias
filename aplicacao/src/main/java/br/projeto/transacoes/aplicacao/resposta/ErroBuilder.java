package br.projeto.transacoes.aplicacao.resposta;

public interface ErroBuilder {

    ErroBuilder comValor(final String valor);

    ErroBuilder campo(final TabelaCampo tabelaCampo);

    ErroBuilder tipoErro(final TipoErro tipoErro);

    Erro build();

}
