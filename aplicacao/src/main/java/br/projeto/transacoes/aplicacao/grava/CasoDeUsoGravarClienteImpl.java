package br.projeto.transacoes.aplicacao.grava;

import br.project.knin.activity.Activity;
import br.project.knin.activity.Channel;
import br.projeto.transacoes.aplicacao.resposta.Erro;
import br.projeto.transacoes.aplicacao.resposta.TabelaCampo;
import br.projeto.transacoes.aplicacao.resposta.TipoErro;
import br.projeto.transacoes.domain.cliente.Cliente;
import br.projeto.transacoes.domain.cliente.ClienteCommand;
import br.projeto.transacoes.domain.cliente.SituacaoCliente;
import br.projeto.transacoes.domain.transacao.Intervalo;
import br.projeto.transacoes.domain.transacao.Transacao;
import br.projeto.transacoes.domain.transacao.TransacaoCommand;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

final class CasoDeUsoGravarClienteImpl implements CasoDeUsoGravarCliente {

    CasoDeUsoGravarClienteImpl
            (
                    final ClienteCommand clienteCommand,
                    final TransacaoCommand transacaoCommand
            ) {
        this.clienteCommand = clienteCommand;
        this.transacaoCommand = transacaoCommand;
    }

    private final ClienteCommand clienteCommand;

    private final TransacaoCommand transacaoCommand;

    @Override
    public void processar
            (
                    final RequisicaoClienteComTransacoes requisicaoClienteComTransacoes,
                    final Channel<Set<Erro>> channelError
            ) {

        Activity
                .contract(Collections.singleton(Erro.mock()))
                .entry(requisicaoClienteComTransacoes)
                .action(requisicao -> requisicao
                        .obterContasBancaria()
                        .stream()
                        .flatMap(RequisicaoContaBancaria::transacoes)
                        .filter
                                (
                                        transacao ->
                                                transacao
                                                        .noIntervalo
                                                                (
                                                                        Intervalo.between
                                                                                (
                                                                                        LocalDate.of(2007, 1, 1),
                                                                                        LocalDate.now()
                                                                                )
                                                                )
                                )
                        .collect(Collectors.toUnmodifiableSet())
                )
                .decision(transacaos -> !transacaos.isEmpty())
                .otherwiseChannel
                        (
                                requisicao ->
                                        Collections.singleton
                                                (
                                                        Erro
                                                                .builder()
                                                                .campo(TabelaCampo.CONTA_CORRENTE)
                                                                .tipoErro(TipoErro.PERIODO_SEM_TRANSACOES)
                                                                .comValor(requisicao.obterCpf())
                                                                .build()
                                                ),
                                channelError
                        )
                .channel
                        (
                                (requisicao, transacaos) ->
                                        Cliente
                                                .builder()
                                                .comCpf(requisicao.obterCpf())
                                                .situacao(SituacaoCliente.ATIVADO)
                                                .possuiContas
                                                        (
                                                                transacaos
                                                                        .stream()
                                                                        .map(Transacao::obterContaBancaria)
                                                                        .collect(Collectors.toUnmodifiableSet())
                                                        )
                                                .build(),
                                clienteCommand::salvar
                        )
                .channel(transacaoCommand::salvar);

    }

}
