package br.projeto.transacoes.aplicacao.consulta;

import br.projeto.transacoes.aplicacao.resposta.TabelaStatus;
import br.projeto.transacoes.domain.cliente.Cliente;
import br.projeto.transacoes.domain.cliente.ClienteQuery;
import br.projeto.transacoes.domain.transacao.Transacao;
import br.projeto.transacoes.domain.transacao.TransacaoQuery;

@FunctionalInterface
public interface CasoDeUsoApresentarTransacoesBancaria {

    static CasoDeUsoApresentarTransacoesBancaria create
            (
                    final ClienteQuery clienteQuery,
                    final TransacaoQuery transacaoQuery,
                    final TabelaStatus tabelaStatus
            ) {
        return
                new CasoDeUsoApresentarTransacoesBancariaProxyValidacao
                        (
                                new CasoDeUsoApresentarTransacoesBancariaImpl
                                        (
                                                clienteQuery,
                                                transacaoQuery,
                                                tabelaStatus
                                        ),
                                tabelaStatus
                        );

    }

    static CasoDeUsoApresentarTransacoesBancaria nullable(final int status) {
        return requisicaoConsulta -> RespostaConsulta.nullable(status);
    }

    static CasoDeUsoApresentarTransacoesBancaria mock
            (
                    final Cliente cliente,
                    final Transacao transacao,
                    final TabelaStatus tabelaStatus
            ) {
        return create
                (
                        ClienteQuery.mock(cliente),
                        TransacaoQuery.mock(transacao),
                        tabelaStatus
                );
    }

    RespostaConsulta consultarTransacoes(final RequisicaoConsulta requisicaoConsulta);

}
