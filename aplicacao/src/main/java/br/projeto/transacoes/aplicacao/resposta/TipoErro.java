package br.projeto.transacoes.aplicacao.resposta;

public enum TipoErro {

    INVALIDO(1, "Campo invalido"),
    CLIENTE_NAO_ENCONTRADO(2, "Cliente não encontrado"),
    CLIENTE_NAO_AUTORIZADO(3, "Cliente não autorizado a consulta de transações"),
    CONTA_BANCARIA_NAO_ENCONTRADA(4, "Não foi possível encontrar conta bancária com dados informados"),
    PERIODO_SEM_TRANSACOES(5, "Informações de transações para o período informado não foi encontrada");

    TipoErro(final int code, final String message) {
        this.code = code;
        this.message = message;
    }

    private final String message;

    private final int code;

    public int toInt() {
        return code;
    }

    @Override
    public String toString() {
        return message;
    }

}
