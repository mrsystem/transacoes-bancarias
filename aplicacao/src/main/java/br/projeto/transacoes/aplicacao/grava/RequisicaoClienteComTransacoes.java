package br.projeto.transacoes.aplicacao.grava;

import br.projeto.transacoes.domain.cliente.Cliente;
import br.projeto.transacoes.domain.conta.ContaBancaria;

import java.util.Collection;
import java.util.Set;

public interface RequisicaoClienteComTransacoes {

    class RequisicaoClienteComTransacoesSkeletal implements RequisicaoClienteComTransacoes {

        RequisicaoClienteComTransacoesSkeletal(final Cliente cliente) {
            this.cliente = cliente;
        }

        private final Cliente cliente;

        @Override
        public String obterCpf() {
            return cliente.obterCpf();
        }

        @Override
        public Collection<RequisicaoContaBancaria> obterContasBancaria() {
            return Set.of(RequisicaoContaBancaria.mock(cliente.obterContasBancaria().iterator().next()));
        }

    }

    static RequisicaoClienteComTransacoes mock(final Cliente cliente) {
        return new RequisicaoClienteComTransacoesSkeletal(cliente);
    }

    String obterCpf();

    Collection<RequisicaoContaBancaria> obterContasBancaria();

}
