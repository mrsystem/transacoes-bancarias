package br.projeto.transacoes.aplicacao.resposta;

public enum TabelaCampo {

    DATA_INICIO("Data Inicio"),
    DATA_FIM("Data Fim"),
    INTERVALO("Intervalo"),
    AGENCIA("Agência"),
    CONTA_CORRENTE("Conta Corrente"),
    CPF("Cadastro de pessoa física"),
    VALOR("Valor de transação");

    TabelaCampo(final String mensagem) {
        this.mensagem = mensagem;
    }

    private final String mensagem;

    @Override
    public String toString() {
        return mensagem;
    }

}
