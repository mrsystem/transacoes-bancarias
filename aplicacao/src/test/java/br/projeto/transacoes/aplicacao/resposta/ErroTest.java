package br.projeto.transacoes.aplicacao.resposta;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ErroTest {

    @Test
    void deveConstruirError() {

        final TabelaCampo campo = TabelaCampo.CPF;

        final TipoErro tipoErro = TipoErro.INVALIDO;

        final String value = "123";

        final Erro erro = Erro.mock();

        Assertions.assertEquals(campo.toString(), erro.obterCampo());

        Assertions
                .assertEquals(tipoErro.toInt(), erro.obterCodigoErro());

        Assertions
                .assertEquals(tipoErro.toString(), erro.obterMensagem());

        Assertions
                .assertEquals(value, erro.obterValor());

    }

}