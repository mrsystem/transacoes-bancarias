package br.projeto.transacoes.aplicacao.consulta;

import br.projeto.transacoes.aplicacao.resposta.TabelaStatus;
import br.projeto.transacoes.domain.cliente.Cliente;
import br.projeto.transacoes.domain.cliente.SituacaoCliente;
import br.projeto.transacoes.domain.transacao.CategoriaTransacao;
import br.projeto.transacoes.domain.transacao.Transacao;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Set;

class CasoDeUsoApresentarTransacoesBancariaTest {

    @Test
    void deveRetornarRespostaConsultaNula() {

        final TabelaStatus tabelaStatus = TabelaStatus.mock();

        final CasoDeUsoApresentarTransacoesBancaria casoDeUsoApresentarTransacoesBancaria =

                CasoDeUsoApresentarTransacoesBancaria.nullable(tabelaStatus.statusSuccess());

        final RespostaConsulta respostaConsulta = casoDeUsoApresentarTransacoesBancaria
                .consultarTransacoes(RequisicaoConsulta.mock(Cliente.mock(), LocalDate.now()));

        Assertions.assertEquals(tabelaStatus.statusSuccess(), respostaConsulta.obterStatus());

        Assertions.assertTrue(respostaConsulta.obterErros().isEmpty());

        Assertions.assertTrue(respostaConsulta.obterTransacoes().isEmpty());

    }

    @Test
    void deveRetornarRespostaConsultaParaMock() {

        final TabelaStatus tabelaStatus = TabelaStatus.mock();

        final LocalDateTime now = LocalDateTime.now();

        final CasoDeUsoApresentarTransacoesBancaria casoDeUsoApresentarTransacoesBancaria =
                CasoDeUsoApresentarTransacoesBancaria.mock
                        (
                                Cliente
                                        .builder(Cliente.mock())
                                        .situacao(SituacaoCliente.ATIVADO)
                                        .build(),
                                Transacao.mock(now),
                                tabelaStatus
                        );

        final RespostaConsulta respostaConsulta = casoDeUsoApresentarTransacoesBancaria
                .consultarTransacoes(RequisicaoConsulta.mock(Cliente.mock(), now.toLocalDate().minusDays(1)));

        Assertions.assertEquals(tabelaStatus.statusSuccess(), respostaConsulta.obterStatus());

        Assertions.assertTrue(respostaConsulta.obterErros().isEmpty());

        final Map<CategoriaTransacao, Set<RespostaTransacao>> transacoes = respostaConsulta.obterTransacoes();

        Assertions.assertFalse(transacoes.isEmpty());

        Assertions.assertEquals(3, transacoes.size());

        Assertions.assertTrue(transacoes.containsKey(CategoriaTransacao.TAXA));

    }

}
