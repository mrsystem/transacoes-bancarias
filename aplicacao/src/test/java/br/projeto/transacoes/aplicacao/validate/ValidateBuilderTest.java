package br.projeto.transacoes.aplicacao.validate;

import br.projeto.transacoes.aplicacao.resposta.Erro;
import br.projeto.transacoes.aplicacao.resposta.TabelaCampo;
import br.projeto.transacoes.aplicacao.resposta.TipoErro;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Set;

class ValidateBuilderTest {

    @Test
    void semErroValidatorDeveRetornarUmaColecaoVazia() {

        final Set<Erro> erros = ValidateBuilder
                .init()
                .ifInvalid(() -> 1, () -> "1", integer -> integer <= 1, TabelaCampo.AGENCIA)
                .build();

        Assertions
                .assertTrue(erros.isEmpty());

    }

    @Test
    void comErroValidatorDeveRetornarUmaColecaoComUmItem() {

        final TabelaCampo tabelaCampo = TabelaCampo.AGENCIA;

        final String value = "1";

        final TipoErro tipoErro = TipoErro.INVALIDO;

        final Set<Erro> erros = ValidateBuilder
                .init()
                .ifInvalid(() -> 1, () -> "1", integer -> integer < 1, tabelaCampo)
                .build();

        Assertions
                .assertFalse(erros.isEmpty());

        Assertions.assertEquals(1, erros.size());

        final Erro erro = erros.iterator().next();

        Assertions.assertEquals(tabelaCampo.toString(), erro.obterCampo());

        Assertions
                .assertEquals(tipoErro.toInt(), erro.obterCodigoErro());

        Assertions
                .assertEquals(tipoErro.toString(), erro.obterMensagem());

        Assertions
                .assertEquals(value, erro.obterValor());

    }

    @Test
    void deveRetornarUmaColecaoComTresErros() {

        final Set<Erro> erros = ValidateBuilder
                .init()
                .ifInvalid(() -> -2, () -> "1", integer -> integer > 1, TabelaCampo.CONTA_CORRENTE)
                .ifInvalid(() -> -1, () -> "1", integer -> integer > 1, TabelaCampo.CPF)
                .ifInvalid(() -> 0, () -> "1", integer -> integer > 1, TabelaCampo.DATA_FIM)
                .build();

        Assertions
                .assertFalse(erros.isEmpty());

        Assertions.assertEquals(3, erros.size());


    }
}