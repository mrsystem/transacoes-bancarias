package br.projeto.transacoes.aplicacao.grava;

import br.projeto.transacoes.aplicacao.resposta.Erro;
import br.projeto.transacoes.aplicacao.resposta.TabelaCampo;
import br.projeto.transacoes.aplicacao.resposta.TipoErro;
import br.projeto.transacoes.domain.cliente.Cliente;
import br.projeto.transacoes.domain.cliente.SituacaoCliente;
import br.projeto.transacoes.domain.conta.ContaBancaria;
import br.projeto.transacoes.domain.transacao.Transacao;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Collection;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

import static br.projeto.transacoes.aplicacao.AssertErro.assertErro;

class CasoDeUsoGravarClienteImplTest {

    @Test
    void deveRetornarQueParaPeriodoNaoHaTransacoes() {

        final AtomicReference<Set<Erro>> atomicReference = new AtomicReference<>();

        final CasoDeUsoGravarCliente casoDeUsoGravarCliente
                = CasoDeUsoGravarCliente.create
                (
                        cliente -> {
                            throw new IllegalCallerException("Não deveria salvar dados de cliente");
                        },
                        transacoes -> {
                            throw new IllegalCallerException("Não deveria salvar dados de transações");
                        }
                );

        final Cliente cliente = Cliente.mock();

        casoDeUsoGravarCliente
                .processar
                        (
                                new RequisicaoClienteComTransacoes.RequisicaoClienteComTransacoesSkeletal(cliente) {
                                    @Override
                                    public Collection<RequisicaoContaBancaria> obterContasBancaria() {
                                        return Set.of
                                                (
                                                        new RequisicaoContaBancaria.RequisicaoContaBancariaSkeletal(ContaBancaria.mock()) {
                                                            @Override
                                                            public Collection<RequisicaoTransacao> obterRequisicaoTransacoes() {
                                                                return Set.of
                                                                        (
                                                                                new RequisicaoTransacao
                                                                                        .RequisicaoTransacaoSkeletal
                                                                                        (
                                                                                                Transacao.mock
                                                                                                        (
                                                                                                                LocalDateTime.of
                                                                                                                        (
                                                                                                                                LocalDate.of(2006, 1, 1),
                                                                                                                                LocalTime.now()
                                                                                                                        )
                                                                                                        )
                                                                                        )
                                                                        );
                                                            }
                                                        }
                                                );
                                    }
                                },
                                atomicReference::set
                        );


        final Set<Erro> erros = atomicReference.get();

        Assertions.assertNotNull(erros);

        Assertions.assertFalse(erros.isEmpty());

        Assertions.assertEquals(1, erros.size());

        final Erro erro = erros.iterator().next();

        assertErro(cliente.obterCpf(), TabelaCampo.CONTA_CORRENTE, erro, TipoErro.PERIODO_SEM_TRANSACOES);
        
    }

    @Test
    void deveGravarInformacoesDeCliente() {

        final AtomicReference<Cliente> clienteAtomicReference = new AtomicReference<>();

        final AtomicReference<Collection<Transacao>> collectionAtomicReference = new AtomicReference<>();

        final CasoDeUsoGravarCliente casoDeUsoGravarCliente =
                CasoDeUsoGravarCliente.create(clienteAtomicReference::set, collectionAtomicReference::set);

        final Cliente clienteMock = Cliente.mock();

        casoDeUsoGravarCliente
                .processar
                        (
                                RequisicaoClienteComTransacoes.mock(clienteMock),
                                erros ->
                                {
                                    throw new IllegalCallerException("Não deveria encaminhar erros para o canal");
                                }
                        );

        final Cliente cliente = clienteAtomicReference.get();

        Assertions.assertNotNull(cliente);

        final Collection<Transacao> transacaos = collectionAtomicReference.get();

        Assertions.assertNotNull(transacaos);

        Assertions.assertEquals(clienteMock.obterCpf(), cliente.obterCpf());

        final ContaBancaria contaBancariaMock = ContaBancaria.mock();

        final Optional<ContaBancaria> contaBancariaOptional =
                cliente.contaBancaria(contaBancariaMock.getAgencia(), contaBancariaMock.getContaCorrente());

        Assertions.assertTrue(contaBancariaOptional.isPresent());

        final ContaBancaria contaBancaria = contaBancariaOptional.get();

        Assertions.assertEquals(contaBancariaMock, contaBancaria);

        Assertions.assertEquals(SituacaoCliente.ATIVADO, cliente.obterSituacao());

        Assertions.assertFalse(transacaos.isEmpty());

        Assertions.assertEquals(1, transacaos.size());

        final Transacao transacao = transacaos.iterator().next();

        final Transacao transacaoMock = Transacao.mock();

        Assertions.assertEquals(transacaoMock.obterValor(), transacao.obterValor());

        Assertions.assertEquals(transacaoMock.obterDescricao(), transacao.obterDescricao());

        Assertions.assertFalse(transacao.obterAutenticacao().isEmpty());

    }
}