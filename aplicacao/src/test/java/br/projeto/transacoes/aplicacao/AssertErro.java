package br.projeto.transacoes.aplicacao;

import br.projeto.transacoes.aplicacao.resposta.Erro;
import br.projeto.transacoes.aplicacao.resposta.TabelaCampo;
import br.projeto.transacoes.aplicacao.resposta.TipoErro;
import org.junit.jupiter.api.Assertions;

public class AssertErro {

    public static void assertErro(final String value, final TabelaCampo tabelaCampo, final Erro erro, TipoErro tipoErro) {

        Assertions.assertEquals(tabelaCampo.toString(), erro.obterCampo());

        Assertions.assertEquals(tipoErro.toString(), erro.obterMensagem());

        Assertions.assertEquals(tipoErro.toInt(), erro.obterCodigoErro());

        Assertions.assertEquals(value, erro.obterValor());

    }

    private AssertErro() {

    }
}
