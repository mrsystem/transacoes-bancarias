package br.projeto.transacoes.aplicacao.resposta;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ErroBuilderTest {

    @Test
    void deveConstruirError() {

        final TabelaCampo campo = TabelaCampo.AGENCIA;

        final TipoErro tipoErro = TipoErro.INVALIDO;

        final String value = "value";

        final Erro erro =
                Erro
                        .builder()
                        .tipoErro(tipoErro)
                        .comValor(value)
                        .campo(campo)
                        .build();

        Assertions.assertEquals(campo.toString(), erro.obterCampo());

        Assertions
                .assertEquals(tipoErro.toInt(), erro.obterCodigoErro());

        Assertions
                .assertEquals(tipoErro.toString(), erro.obterMensagem());

        Assertions
                .assertEquals(value, erro.obterValor());

    }
}