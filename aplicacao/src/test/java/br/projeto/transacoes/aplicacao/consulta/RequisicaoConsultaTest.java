package br.projeto.transacoes.aplicacao.consulta;

import br.projeto.transacoes.domain.cliente.Cliente;
import br.projeto.transacoes.domain.conta.ContaBancaria;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Optional;

class RequisicaoConsultaTest {

    @Test
    void deveConstruirUmaImplementacaoSkeletal() {

        final Cliente cliente = Cliente.mock();

        final RequisicaoConsulta requisicaoConsulta = RequisicaoConsulta.mock(cliente, LocalDate.now());

        Assertions.assertNotNull(requisicaoConsulta);

        final Optional<ContaBancaria> optional =
                cliente.contaBancaria(requisicaoConsulta.obterAgencia(), requisicaoConsulta.obterContaCorrente());

        Assertions.assertTrue(optional.isPresent());

        final ContaBancaria contaBancaria = optional.get();

        Assertions.assertEquals(requisicaoConsulta.obterAgencia(), contaBancaria.getAgencia());

        Assertions
                .assertEquals(requisicaoConsulta.obterContaCorrente(), contaBancaria.getContaCorrente());

    }
}