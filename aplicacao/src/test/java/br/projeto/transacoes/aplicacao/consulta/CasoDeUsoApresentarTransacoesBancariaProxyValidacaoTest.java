package br.projeto.transacoes.aplicacao.consulta;

import br.projeto.transacoes.aplicacao.resposta.Erro;
import br.projeto.transacoes.aplicacao.resposta.TabelaCampo;
import br.projeto.transacoes.aplicacao.resposta.TabelaStatus;
import br.projeto.transacoes.aplicacao.resposta.TipoErro;
import br.projeto.transacoes.domain.cliente.Cliente;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Iterator;
import java.util.Set;

import static br.projeto.transacoes.aplicacao.AssertErro.assertErro;

class CasoDeUsoApresentarTransacoesBancariaProxyValidacaoTest {

    @Test
    void deveRetornarCpfInvalido() {

        final String value = "2374327243234";

        final TabelaCampo cpf = TabelaCampo.CPF;

        final TabelaStatus tabelaStatus = TabelaStatus.mock();

        final CasoDeUsoApresentarTransacoesBancariaProxyValidacao validacao =
                new CasoDeUsoApresentarTransacoesBancariaProxyValidacao(null, tabelaStatus);

        final RespostaConsulta respostaConsulta = validacao
                .consultarTransacoes(new RequisicaoConsulta.SkeletalRequisicaoConsulta(Cliente.mock(), LocalDate.now()) {
                    @Override
                    public String obterCpf() {
                        return value;
                    }
                });

        Assertions.assertTrue(respostaConsulta.obterTransacoes().isEmpty());

        Assertions.assertEquals(tabelaStatus.statusRequestInvalid(), respostaConsulta.obterStatus());

        final Set<Erro> erros = respostaConsulta.obterErros();

        Assertions.assertFalse(erros.isEmpty());

        Assertions.assertEquals(1, erros.size());

        final Erro erro = erros.iterator().next();

        assertErro(value, cpf, erro, TipoErro.INVALIDO);

    }

    @Test
    void deveRetornarDataInicialInvalida() {

        final String value = "null";

        final TabelaCampo dataInicio = TabelaCampo.DATA_INICIO;

        final TabelaStatus tabelaStatus = TabelaStatus.mock();

        final CasoDeUsoApresentarTransacoesBancariaProxyValidacao validacao =
                new CasoDeUsoApresentarTransacoesBancariaProxyValidacao(null, tabelaStatus);

        RequisicaoConsulta.SkeletalRequisicaoConsulta requisicaoConsulta =
                new RequisicaoConsulta.SkeletalRequisicaoConsulta(Cliente.mock(), LocalDate.now()) {
                    @Override
                    public String obterDataInicio() {
                        return value;
                    }
                };

        final RespostaConsulta respostaConsulta = validacao
                .consultarTransacoes(requisicaoConsulta);

        Assertions.assertTrue(respostaConsulta.obterTransacoes().isEmpty());

        Assertions.assertEquals(tabelaStatus.statusRequestInvalid(), respostaConsulta.obterStatus());

        final Set<Erro> erros = respostaConsulta.obterErros();

        Assertions.assertFalse(erros.isEmpty());

        Assertions.assertEquals(2, erros.size());

        final Iterator<Erro> iterator = erros.iterator();

        final Erro erroDataInicio = iterator.next();

        Assertions.assertEquals(dataInicio.toString(), erroDataInicio.obterCampo());

        final TipoErro tipoErro = TipoErro.INVALIDO;

        Assertions.assertEquals(tipoErro.toString(), erroDataInicio.obterMensagem());

        Assertions.assertEquals(tipoErro.toInt(), erroDataInicio.obterCodigoErro());

        Assertions.assertEquals(value, erroDataInicio.obterValor());

        //

        final Erro erroPeriodo = iterator.next();

        Assertions.assertEquals(TabelaCampo.INTERVALO.toString(), erroPeriodo.obterCampo());

        Assertions.assertEquals(tipoErro.toString(), erroPeriodo.obterMensagem());

        Assertions.assertEquals(tipoErro.toInt(), erroPeriodo.obterCodigoErro());

        Assertions.assertEquals(requisicaoConsulta.obterDataInicioFim(), erroPeriodo.obterValor());

    }

    @Test
    void deveRetornarDataFimInvalida() {

        final String value = "null";

        final TabelaStatus tabelaStatus = TabelaStatus.mock();

        final TabelaCampo dataFim = TabelaCampo.DATA_FIM;

        final CasoDeUsoApresentarTransacoesBancariaProxyValidacao validacao =
                new CasoDeUsoApresentarTransacoesBancariaProxyValidacao(null, tabelaStatus);

        RequisicaoConsulta.SkeletalRequisicaoConsulta requisicaoConsulta =
                new RequisicaoConsulta.SkeletalRequisicaoConsulta(Cliente.mock(), LocalDate.now()) {
                    @Override
                    public String obterDataFim() {
                        return value;
                    }
                };

        final RespostaConsulta respostaConsulta = validacao
                .consultarTransacoes(requisicaoConsulta);

        Assertions.assertTrue(respostaConsulta.obterTransacoes().isEmpty());

        Assertions.assertEquals(tabelaStatus.statusRequestInvalid(), respostaConsulta.obterStatus());

        final Set<Erro> erros = respostaConsulta.obterErros();

        Assertions.assertFalse(erros.isEmpty());

        Assertions.assertEquals(2, erros.size());

        final Iterator<Erro> iterator = erros.iterator();

        final Erro erroDataFim = iterator.next();

        Assertions.assertEquals(dataFim.toString(), erroDataFim.obterCampo());

        final TipoErro tipoErro = TipoErro.INVALIDO;

        Assertions.assertEquals(tipoErro.toString(), erroDataFim.obterMensagem());

        Assertions.assertEquals(tipoErro.toInt(), erroDataFim.obterCodigoErro());

        Assertions.assertEquals(value, erroDataFim.obterValor());

        //

        final Erro erroPeriodo = iterator.next();

        Assertions.assertEquals(TabelaCampo.INTERVALO.toString(), erroPeriodo.obterCampo());

        Assertions.assertEquals(tipoErro.toString(), erroPeriodo.obterMensagem());

        Assertions.assertEquals(tipoErro.toInt(), erroPeriodo.obterCodigoErro());

        Assertions.assertEquals(requisicaoConsulta.obterDataInicioFim(), erroPeriodo.obterValor());

    }

    @Test
    void deveRetornarAgenciaInvalida() {

        final String value = "null";

        final TabelaCampo agencia = TabelaCampo.AGENCIA;

        final TabelaStatus tabelaStatus = TabelaStatus.mock();

        final CasoDeUsoApresentarTransacoesBancariaProxyValidacao validacao =
                new CasoDeUsoApresentarTransacoesBancariaProxyValidacao(null, tabelaStatus);

        final RespostaConsulta respostaConsulta = validacao
                .consultarTransacoes(new RequisicaoConsulta.SkeletalRequisicaoConsulta(Cliente.mock(), LocalDate.now()) {
                    @Override
                    public String obterAgencia() {
                        return value;
                    }
                });

        Assertions.assertTrue(respostaConsulta.obterTransacoes().isEmpty());

        Assertions.assertEquals(tabelaStatus.statusRequestInvalid(), respostaConsulta.obterStatus());

        final Set<Erro> erros = respostaConsulta.obterErros();

        Assertions.assertFalse(erros.isEmpty());

        Assertions.assertEquals(1, erros.size());

        final Erro erro = erros.iterator().next();

        assertErro(value, agencia, erro, TipoErro.INVALIDO);

    }

    @Test
    void deveRetornarContaCorrenteInvalida() {

        final String value = "null";

        final TabelaCampo contaBancaria = TabelaCampo.CONTA_CORRENTE;

        final TabelaStatus tabelaStatus = TabelaStatus.mock();

        final CasoDeUsoApresentarTransacoesBancariaProxyValidacao validacao =
                new CasoDeUsoApresentarTransacoesBancariaProxyValidacao(null, tabelaStatus);

        final RespostaConsulta respostaConsulta = validacao
                .consultarTransacoes(new RequisicaoConsulta.SkeletalRequisicaoConsulta(Cliente.mock(), LocalDate.now()) {
                    @Override
                    public String obterContaCorrente() {
                        return value;
                    }
                });

        Assertions.assertTrue(respostaConsulta.obterTransacoes().isEmpty());

        Assertions.assertEquals(tabelaStatus.statusRequestInvalid(), respostaConsulta.obterStatus());

        final Set<Erro> erros = respostaConsulta.obterErros();

        Assertions.assertFalse(erros.isEmpty());

        Assertions.assertEquals(1, erros.size());

        final Erro erro = erros.iterator().next();

        assertErro(value, contaBancaria, erro, TipoErro.INVALIDO);

    }

    @Test
    void deveRetornarRespostaNula() {

        final TabelaStatus tabelaStatus = TabelaStatus.mock();

        final CasoDeUsoApresentarTransacoesBancariaProxyValidacao validacao =
                new CasoDeUsoApresentarTransacoesBancariaProxyValidacao(CasoDeUsoApresentarTransacoesBancaria.nullable(tabelaStatus.statusSuccess()), tabelaStatus);

        final RespostaConsulta respostaConsulta = validacao
                .consultarTransacoes(RequisicaoConsulta.mock(Cliente.mock(), LocalDate.now()));

        Assertions.assertTrue(respostaConsulta.obterTransacoes().isEmpty());

        Assertions.assertEquals(tabelaStatus.statusSuccess(), respostaConsulta.obterStatus());

        Assertions.assertTrue(respostaConsulta.obterErros().isEmpty());

    }
}