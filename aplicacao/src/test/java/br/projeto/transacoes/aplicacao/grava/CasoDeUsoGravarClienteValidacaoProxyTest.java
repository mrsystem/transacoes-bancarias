package br.projeto.transacoes.aplicacao.grava;

import br.projeto.transacoes.aplicacao.resposta.Erro;
import br.projeto.transacoes.aplicacao.resposta.TabelaCampo;
import br.projeto.transacoes.aplicacao.resposta.TipoErro;
import br.projeto.transacoes.domain.cliente.Cliente;
import br.projeto.transacoes.domain.conta.ContaBancaria;
import br.projeto.transacoes.domain.transacao.Transacao;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

import static br.projeto.transacoes.aplicacao.AssertErro.assertErro;

class CasoDeUsoGravarClienteValidacaoProxyTest {

    private static final CasoDeUsoGravarCliente CASO_DE_USO_GRAVAR_CLIENTE =
            CasoDeUsoGravarCliente.create(cliente -> {
                throw new IllegalCallerException();
            }, transacoes -> {
                throw new IllegalCallerException();
            });

    @Test
    void quandoInstanciaDeRequisicaoNulaDeveSubirExcecao() {
        Assertions.assertThrows
                (
                        NullPointerException.class,
                        () -> CASO_DE_USO_GRAVAR_CLIENTE.processar(null, null)
                );
    }


    @Test
    void quandoInstanciaDeCanalNulaDeveSubirExcecao() {

        Assertions.assertThrows
                (
                        NullPointerException.class,
                        () -> CASO_DE_USO_GRAVAR_CLIENTE.processar(RequisicaoClienteComTransacoes.mock(Cliente.mock()), null)
                );

    }


    @Test
    void quandoCpfDeClienteInvalidoDeveRetornarErro() {

        final AtomicReference<Set<Erro>> atomicReference = new AtomicReference<>();

        final String cpf = "";

        CASO_DE_USO_GRAVAR_CLIENTE
                .processar
                        (
                                new RequisicaoClienteComTransacoes.RequisicaoClienteComTransacoesSkeletal(Cliente.mock()) {
                                    @Override
                                    public String obterCpf() {
                                        return cpf;
                                    }
                                },
                                atomicReference::set
                        );

        final Set<Erro> erros = atomicReference.get();

        Assertions.assertNotNull(erros);

        Assertions.assertFalse(erros.isEmpty());

        Assertions.assertEquals(1, erros.size());

        final Erro erro = erros.iterator().next();

        assertErro(cpf, TabelaCampo.CPF, erro, TipoErro.INVALIDO);

    }

    @Test
    void quandoAgenciaInvalidaDeveRetornarErro() {

        final AtomicReference<Set<Erro>> atomicReference = new AtomicReference<>();

        final String agencia = "";

        CASO_DE_USO_GRAVAR_CLIENTE
                .processar
                        (
                                new RequisicaoClienteComTransacoes.RequisicaoClienteComTransacoesSkeletal(Cliente.mock()) {
                                    @Override
                                    public Collection<RequisicaoContaBancaria> obterContasBancaria() {
                                        return Set.of(new RequisicaoContaBancaria.RequisicaoContaBancariaSkeletal(ContaBancaria.mock()) {
                                            @Override
                                            public String obterAgencia() {
                                                return agencia;
                                            }
                                        });
                                    }
                                },
                                atomicReference::set
                        );

        final Set<Erro> erros = atomicReference.get();

        Assertions.assertNotNull(erros);

        Assertions.assertFalse(erros.isEmpty());

        Assertions.assertEquals(1, erros.size());

        final Erro erro = erros.iterator().next();

        assertErro(agencia, TabelaCampo.AGENCIA, erro, TipoErro.INVALIDO);

    }

    @Test
    void quandoContaCorrenteInvalidaDeveRetornarErro() {

        final AtomicReference<Set<Erro>> atomicReference = new AtomicReference<>();

        final String contaCorrente = "";

        CASO_DE_USO_GRAVAR_CLIENTE
                .processar
                        (
                                new RequisicaoClienteComTransacoes.RequisicaoClienteComTransacoesSkeletal(Cliente.mock()) {
                                    @Override
                                    public Collection<RequisicaoContaBancaria> obterContasBancaria() {
                                        return Set.of(new RequisicaoContaBancaria.RequisicaoContaBancariaSkeletal(ContaBancaria.mock()) {
                                            @Override
                                            public String obterContaCorrente() {
                                                return contaCorrente;
                                            }
                                        });
                                    }
                                },
                                atomicReference::set
                        );

        final Set<Erro> erros = atomicReference.get();

        Assertions.assertNotNull(erros);

        Assertions.assertFalse(erros.isEmpty());

        Assertions.assertEquals(1, erros.size());

        final Erro erro = erros.iterator().next();

        assertErro(contaCorrente, TabelaCampo.CONTA_CORRENTE, erro, TipoErro.INVALIDO);

    }

    @Test
    void quandoValorTransacaoInvalidoDeveRetornarErro() {

        final AtomicReference<Set<Erro>> atomicReference = new AtomicReference<>();

        final BigDecimal valor = BigDecimal.valueOf(-1);

        CASO_DE_USO_GRAVAR_CLIENTE
                .processar
                        (
                                new RequisicaoClienteComTransacoes.RequisicaoClienteComTransacoesSkeletal(Cliente.mock()) {
                                    @Override
                                    public Collection<RequisicaoContaBancaria> obterContasBancaria() {
                                        return Set.of(new RequisicaoContaBancaria.RequisicaoContaBancariaSkeletal(ContaBancaria.mock()) {
                                            @Override
                                            public Collection<RequisicaoTransacao> obterRequisicaoTransacoes() {
                                                return Set.of
                                                        (
                                                                new RequisicaoTransacao.RequisicaoTransacaoSkeletal(Transacao.mock()) {
                                                                    @Override
                                                                    public BigDecimal obterValor() {
                                                                        return valor;
                                                                    }
                                                                }
                                                        );
                                            }
                                        });
                                    }
                                },
                                atomicReference::set
                        );

        final Set<Erro> erros = atomicReference.get();

        Assertions.assertNotNull(erros);

        Assertions.assertFalse(erros.isEmpty());

        Assertions.assertEquals(1, erros.size());

        final Erro erro = erros.iterator().next();

        assertErro(valor.toString(), TabelaCampo.VALOR, erro, TipoErro.INVALIDO);

    }

}