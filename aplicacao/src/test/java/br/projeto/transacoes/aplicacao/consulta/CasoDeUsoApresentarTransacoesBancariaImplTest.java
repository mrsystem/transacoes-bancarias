package br.projeto.transacoes.aplicacao.consulta;

import br.projeto.transacoes.aplicacao.resposta.*;
import br.projeto.transacoes.domain.cliente.Cliente;
import br.projeto.transacoes.domain.cliente.ClienteQuery;
import br.projeto.transacoes.domain.cliente.SituacaoCliente;
import br.projeto.transacoes.domain.conta.ContaBancaria;
import br.projeto.transacoes.domain.transacao.CategoriaTransacao;
import br.projeto.transacoes.domain.transacao.Transacao;
import br.projeto.transacoes.domain.transacao.TransacaoQuery;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

class CasoDeUsoApresentarTransacoesBancariaImplTest {

    @Test
    @DisplayName("Cenário deve agrupar 'taxa', 'compra' e 'transferência' ordenada por data de efetivação")
    void deveRetornarRespostaDeTransacoesAgrupadasPorCategoriasEOrdenadaPorDataEfetivacao() {

        final Cliente cliente =
                Cliente
                        .builder(Cliente.mock())
                        .situacao(SituacaoCliente.ATIVADO)
                        .build();

        final TabelaStatus tabelaStatus = TabelaStatus.mock();

        final LocalDateTime now = LocalDateTime.now();

        final Transacao transacao = Transacao.mock(now);

        final Set<Transacao> transactions = Transacao.mockAll(transacao);

        final CasoDeUsoApresentarTransacoesBancaria casoDeUsoApresentarTransacoesBancaria
                = new CasoDeUsoApresentarTransacoesBancariaImpl
                (
                        ClienteQuery.mock(cliente),
                        contaBancaria -> transactions,
                        tabelaStatus
                );

        final RespostaConsulta respostaConsulta =
                casoDeUsoApresentarTransacoesBancaria
                        .consultarTransacoes(RequisicaoConsulta.mock(cliente, now.toLocalDate().minusDays(1)));

        Assertions.assertEquals(tabelaStatus.statusSuccess(), respostaConsulta.obterStatus());

        Assertions.assertTrue(respostaConsulta.obterErros().isEmpty());

        final Map<CategoriaTransacao, Set<RespostaTransacao>> transactionMap = respostaConsulta.obterTransacoes();

        Assertions.assertFalse(transactionMap.isEmpty());

        Assertions.assertEquals(3, transactionMap.size());

        Assertions.assertTrue(transactionMap.containsKey(CategoriaTransacao.TAXA));

        Assertions.assertTrue(transactionMap.containsKey(CategoriaTransacao.COMPRA));

        Assertions.assertTrue(transactionMap.containsKey(CategoriaTransacao.TRANSFERENCIA));

        // TAXA

        final Set<RespostaTransacao> taxas = transactionMap.get(CategoriaTransacao.TAXA);

        Assertions.assertFalse(taxas.isEmpty());

        Assertions.assertEquals(2, taxas.size());

        final Iterator<RespostaTransacao> taxaIterator = taxas.iterator();

        Assertions.assertEquals(now.plusDays(1), taxaIterator.next().obterDataHoraTransacao());

        Assertions.assertEquals(now, taxaIterator.next().obterDataHoraTransacao());

        // COMPRA

        final Set<RespostaTransacao> compras = transactionMap.get(CategoriaTransacao.COMPRA);

        Assertions.assertFalse(compras.isEmpty());

        Assertions.assertEquals(2, compras.size());

        final Iterator<RespostaTransacao> comprasIterator = compras.iterator();

        Assertions.assertEquals(now.minusHours(5), comprasIterator.next().obterDataHoraTransacao());

        Assertions.assertEquals(now.minusDays(1), comprasIterator.next().obterDataHoraTransacao());

        // TRANSFERENCIA

        final Set<RespostaTransacao> transferencias = transactionMap.get(CategoriaTransacao.TRANSFERENCIA);

        Assertions.assertFalse(transferencias.isEmpty());

        Assertions.assertEquals(2, transferencias.size());

        final Iterator<RespostaTransacao> transferenciasIterator = transferencias.iterator();

        Assertions.assertEquals(now.minusHours(1), transferenciasIterator.next().obterDataHoraTransacao());

        Assertions.assertEquals(now.minusHours(2), transferenciasIterator.next().obterDataHoraTransacao());

    }

    @Test
    @DisplayName("Cenário com recuperação de transações bancárias")
    void deveRetornarRespostaDeTransacoesBancaria() {

        final Cliente cliente =
                Cliente
                        .builder(Cliente.mock())
                        .situacao(SituacaoCliente.ATIVADO)
                        .build();

        final TabelaStatus tabelaStatus = TabelaStatus.mock();

        final Transacao transacao = Transacao.mock();

        final CasoDeUsoApresentarTransacoesBancaria casoDeUsoApresentarTransacoesBancaria
                = new CasoDeUsoApresentarTransacoesBancariaImpl
                (
                        ClienteQuery.mock(cliente),
                        contaBancaria -> Set.of(Transacao.builder(transacao).paraContaBancaria(contaBancaria).build()),
                        tabelaStatus
                );

        final RespostaConsulta respostaConsulta =
                casoDeUsoApresentarTransacoesBancaria
                        .consultarTransacoes(RequisicaoConsulta.mock(cliente, LocalDate.now()));

        Assertions.assertEquals(tabelaStatus.statusSuccess(), respostaConsulta.obterStatus());

        Assertions.assertTrue(respostaConsulta.obterErros().isEmpty());

        final Map<CategoriaTransacao, Set<RespostaTransacao>> transactionMap = respostaConsulta.obterTransacoes();

        Assertions.assertFalse(transactionMap.isEmpty());

        Assertions.assertEquals(1, transactionMap.size());

        final CategoriaTransacao taxa = CategoriaTransacao.TAXA;

        Assertions.assertTrue(transactionMap.containsKey(taxa));

        final Set<RespostaTransacao> transacaos = transactionMap.get(taxa);

        Assertions.assertFalse(transacaos.isEmpty());

        Assertions.assertEquals(1, transacaos.size());

        final RespostaTransacao respostaTransacao = transacaos.iterator().next();

        Assertions.assertEquals(transacao.obterDataHoraEfetivacao(), respostaTransacao.obterDataHoraTransacao());

        Assertions.assertEquals(transacao.obterDescricao(), respostaTransacao.obterDescricao());

        Assertions.assertEquals(transacao.obterValor(), respostaTransacao.obterValor());

    }

    private static void assertTransaction
            (
                    final RespostaConsulta respostaConsulta,
                    final TabelaCampo tabelaCampo,
                    final TipoErro tipoErro
            ) {

        final Set<Erro> erros = respostaConsulta.obterErros();

        Assertions.assertNotNull(erros);

        Assertions.assertFalse(erros.isEmpty());

        Assertions.assertEquals(1, erros.size());

        final Erro erro = erros.iterator().next();

        Assertions.assertEquals(tabelaCampo.toString(), erro.obterCampo());

        Assertions.assertEquals(tipoErro.toString(), erro.obterMensagem());

        Assertions.assertEquals(tipoErro.toInt(), erro.obterCodigoErro());

    }

    @Test
    @DisplayName("Cenário com transacoes não encontradas para período. Deve retornar resposta")
    void quandoTransacoesNaoEncontradasParaPeriodoInformadoRetornarResposta() {

        final TabelaStatus tabelaStatus = TabelaStatus.mock();

        final Cliente cliente =
                Cliente
                        .builder(Cliente.mock())
                        .situacao(SituacaoCliente.ATIVADO)
                        .build();

        final LocalDateTime now = LocalDateTime.now();

        final Transacao transacao = Transacao.mock(now);

        final CasoDeUsoApresentarTransacoesBancaria casoDeUsoApresentarTransacoesBancaria
                = new CasoDeUsoApresentarTransacoesBancariaImpl
                (
                        cpf -> Optional.of(cliente),
                        TransacaoQuery.mock(transacao),
                        tabelaStatus
                );

        final RespostaConsulta respostaConsulta = casoDeUsoApresentarTransacoesBancaria
                .consultarTransacoes(RequisicaoConsulta.mock(cliente, now.toLocalDate().plusYears(1L)));

        Assertions.assertEquals(tabelaStatus.statusTransactionNotFound(), respostaConsulta.obterStatus());

        final Map<CategoriaTransacao, Set<RespostaTransacao>> transactionMap = respostaConsulta.obterTransacoes();

        Assertions.assertNotNull(transactionMap);

        Assertions.assertTrue(transactionMap.isEmpty());

        assertTransaction(respostaConsulta, TabelaCampo.DATA_INICIO, TipoErro.PERIODO_SEM_TRANSACOES);

    }

    @Test
    @DisplayName("Cenário com conta bancária não recuperada. Deve retornar resposta")
    void quandoContaBancariaNaoEncontradaPorAgenciaEContaCorrenteRetornarResposta() {

        final TabelaStatus tabelaStatus = TabelaStatus.mock();

        final Cliente cliente =
                Cliente
                        .builder(Cliente.mock())
                        .situacao(SituacaoCliente.ATIVADO)
                        .build();

        final CasoDeUsoApresentarTransacoesBancaria casoDeUsoApresentarTransacoesBancaria
                = new CasoDeUsoApresentarTransacoesBancariaImpl
                (
                        cpf -> Optional.of(cliente),
                        contaBancaria -> {
                            throw new IllegalCallerException();
                        },
                        tabelaStatus
                );

        final Cliente clienteComContaCorrenteDiferente =
                Cliente
                        .builder(cliente)
                        .possuiContas(Set.of(ContaBancaria.builder().paraAgencia("9999").comContaCorrente("98765-0").build()))
                        .build();

        final RespostaConsulta respostaConsulta = casoDeUsoApresentarTransacoesBancaria
                .consultarTransacoes(RequisicaoConsulta.mock(clienteComContaCorrenteDiferente, LocalDate.now()));

        Assertions.assertEquals(tabelaStatus.statusBalanceAccountNotFound(), respostaConsulta.obterStatus());

        final Map<CategoriaTransacao, Set<RespostaTransacao>> transactionMap = respostaConsulta.obterTransacoes();

        Assertions.assertNotNull(transactionMap);

        Assertions.assertTrue(transactionMap.isEmpty());

        assertTransaction(respostaConsulta, TabelaCampo.CONTA_CORRENTE, TipoErro.CONTA_BANCARIA_NAO_ENCONTRADA);

    }

    @Test
    @DisplayName("Cenário com cliente não autorizado. Deve retornar resposta")
    void quandoClienteNaoAutorizadoRetornarResposta() {

        final TabelaStatus tabelaStatus = TabelaStatus.mock();

        final Cliente cliente = Cliente.mock();

        final CasoDeUsoApresentarTransacoesBancaria casoDeUsoApresentarTransacoesBancaria
                = new CasoDeUsoApresentarTransacoesBancariaImpl
                (
                        cpf -> Optional.of(cliente),
                        contaBancaria -> {
                            throw new IllegalCallerException();
                        },
                        tabelaStatus
                );


        final RespostaConsulta respostaConsulta = casoDeUsoApresentarTransacoesBancaria
                .consultarTransacoes(RequisicaoConsulta.mock(cliente, LocalDate.now()));

        Assertions.assertEquals(tabelaStatus.statusCustomerNotAuthorized(), respostaConsulta.obterStatus());

        final Map<CategoriaTransacao, Set<RespostaTransacao>> transactionMap = respostaConsulta.obterTransacoes();

        Assertions.assertNotNull(transactionMap);

        Assertions.assertTrue(transactionMap.isEmpty());

        assertTransaction(respostaConsulta, TabelaCampo.CPF, TipoErro.CLIENTE_NAO_AUTORIZADO);

    }

    @Test
    @DisplayName("Cenário com cliente não encontrado. Deve retornar resposta")
    void quandoClienteNaoEncontradoRetornarResposta() {

        final TabelaStatus tabelaStatus = TabelaStatus.mock();

        final CasoDeUsoApresentarTransacoesBancaria casoDeUsoApresentarTransacoesBancaria
                = new CasoDeUsoApresentarTransacoesBancariaImpl
                (
                        cpf -> Optional.empty(),
                        contaBancaria -> {
                            throw new IllegalCallerException();
                        },
                        tabelaStatus
                );

        final Cliente cliente = Cliente.mock();

        final RespostaConsulta respostaConsulta = casoDeUsoApresentarTransacoesBancaria
                .consultarTransacoes(RequisicaoConsulta.mock(cliente, LocalDate.now()));

        Assertions.assertEquals(tabelaStatus.statusCustomerNotFound(), respostaConsulta.obterStatus());

        final Map<CategoriaTransacao, Set<RespostaTransacao>> transactionMap = respostaConsulta.obterTransacoes();

        Assertions.assertNotNull(transactionMap);

        Assertions.assertTrue(transactionMap.isEmpty());

        assertTransaction(respostaConsulta, TabelaCampo.CPF, TipoErro.CLIENTE_NAO_ENCONTRADO);

    }

}